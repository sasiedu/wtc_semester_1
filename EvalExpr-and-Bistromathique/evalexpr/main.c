
#include "libft.h"

char	*get_num(char *str)
{
	int		j;
	char	*temp;

	temp = (char*)malloc(sizeof(char) * 15);
	j = 0;
	while (str[g_i] && (str[g_i] >= '0' && str[g_i] <= '9'))
	{
		temp[j] = str[g_i];
		j++;
		g_i++;
	}
	g_i--;
	return (temp);
}

void	eval_b()
{
	t_stack1	*temp;
	while (g_ops->c != '(')
		eval();
	temp = g_ops->next;
	g_ops = temp;
}

void	eval_ops(char c)
{
	if (c == '-')
		eval_sub();
	else if (c == '+')
		eval_add();
	else if (c == '*')
		eval_mul();
	else if (c == '%')
		eval_mod();
	else if (c == '/')
		eval_div();
	push1(c);
}

int		eval_expr(char *str)
{
	push1('(');
	while (str[g_i])
	{
		if (str[g_i] >= '0' && str[g_i] <= '9')
			push(ft_atoi(get_num(str)));
		else if (str[g_i] == '(')
			push1(str[g_i]);
		else if (str[g_i] == ')')
			eval_b();
		else if (str[g_i] == '+' || str[g_i] == '-' || str[g_i] == '*' \
				|| str[g_i] == '/' || str[g_i] == '%')
			eval_ops(str[g_i]);
		g_i++;
	}
	while (g_ops)
		eval();
	return (g_digit->x);
}

int		main(int ac, char **av)
{
	if (ac > 1)
	{
		ft_putnbr(eval_expr(av[1]));
		ft_putchar('\n');
	}
	return (0);
}
