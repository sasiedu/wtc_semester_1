/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/18 16:40:00 by sasiedu           #+#    #+#             */
/*   Updated: 2016/05/21 14:10:15 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"

void		ft_push_to_keep(const int fd, t_list **keep, char **store)
{
	if ((*keep)->content_size != (size_t)fd)
		ft_push_to_keep(fd, &(*keep)->next, &(*store));
	else
	{
		(*keep)->content = NULL;
		(*keep)->content = ft_strnew(ft_strlen(*store));
		ft_strncpy((*keep)->content, *store, ft_strlen(*store));
	}
}

int			ft_solve(const int fd, char **store, char **line, t_list **keep)
{
	size_t		i;

	i = 0;
	while ((*store)[i] && (*store)[i] != '\n')
		i++;
	if ((*line = ft_strnew(i)) == NULL)
		return (-1);
	ft_memccpy(*line, *store, '\n', ft_strlen(*store));
	(*line)[i] = '\0';
	*store = ft_strchr(*store, '\n');
	if ((*store)[0] == '\n')
		*store = &(*store)[1];
	ft_push_to_keep(fd, &(*keep), &(*store));
	return (0);
}

int			do_read(int fd, char **store)
{
	int		ret;
	char	*temp;
	char	*hold;

	temp = (char *)malloc(sizeof(char) * BUFF_SIZE);
	if (temp == NULL)
		return (-1);
	ret = read(fd, temp, BUFF_SIZE);
	hold = (char *)malloc(sizeof(char) * (BUFF_SIZE + ft_strlen(*store)));
	ft_strncpy(hold, *store, ft_strlen(*store));
	ft_strncat(hold, temp, ft_strlen(temp));
	*store = NULL;
	*store = ft_strnew(ft_strlen(hold) + 1);
	ft_strncpy(*store, hold, ft_strlen(hold));
	(*store)[ft_strlen(*store)] = '\0';
	return (ret);
}

void		ft_get_from_keep(const int fd, char **store, t_list **keep)
{
	if (*keep == NULL)
		*keep = ft_lstnew(*store, fd);
	else if ((*keep)->content_size != (size_t)fd)
		ft_get_from_keep(fd, &(*store), &(*keep)->next);
	else
		*store = (*keep)->content;
}

int			get_next_line(const int fd, char **line)
{
	int				ret;
	char			*store;
	static	t_list	*keep = NULL;

	ret = 1;
	if (fd < 0 || (store = ft_strnew(BUFF_SIZE)) == NULL)
		return (-1);
	ft_get_from_keep(fd, &store, &keep);
	while (ret > 0)
	{
		if ((ft_memchr(store, '\n', ft_strlen(store))) != NULL)
		{
			if (ft_solve(fd, &store, &(*line), &keep) == -1)
				return (-1);
			return (1);
		}
		ret = do_read(fd, &store);
	}
	if (ret <= 0 && (!store || store[0] == '\0'))
		return (0);
	*line = ft_strnew(ft_strlen(store));
	ft_strncpy(*line, store, ft_strlen(store));
	store[0] = '\0';
	ft_push_to_keep(fd, &keep, &store);
	return (1);
}
