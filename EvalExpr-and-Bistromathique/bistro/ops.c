
#include "libft.h"

void	eval_sub()
{
	while (g_ops->c == '+' || g_ops->c == '-' || g_ops->c == '/' \
			|| g_ops->c == '%' || g_ops->c == '*')
		eval();
}

void	eval_add()
{
	while (g_ops->c == '+' || g_ops->c == '/' || g_ops->c == '%' \
			|| g_ops->c == '*')
		eval();
}

void	eval_mul()
{
	while (g_ops->c == '*' || g_ops->c == '/' || g_ops->c == '%')
		eval();
}

void	eval_mod()
{
	while (g_ops->c == '%' || g_ops->c == '/')
		eval();
}

void	eval_div()
{
	while (g_ops->c == '/')
		eval();
}
