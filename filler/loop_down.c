/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loop_down.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/16 00:01:02 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 14:01:18 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include <fcntl.h>

int			loop_up(t_filler **m)
{
	int			h;
	int			w;

	h = (*m)->b_h - 1;
	while (h >= 0)
	{
		w = (*m)->b_w - 1;
		while (w >= 0)
		{
			if ((*m)->b[h][w] == (*m)->pl[0] || (*m)->b[h][w] == \
					(*m)->pl[1])
			{
				if (ft_size1(&(*m), w - (*m)->pi_sw, h - (*m)->pi_sh) == 1)
				{
					if (ft_try1(&(*m), w - (*m)->pi_sw, \
								h - (*m)->pi_sh, 0) == 1)
						return (1);
				}
			}
			w--;
		}
		h--;
	}
	return (0);
}

int		ft_size1(t_filler **mac, int w, int h)
{
	int		space;
	int		fit;

	if (h < 0 || w < 0)
		return (0);
	space = (*mac)->b_w - (w + 1);
	fit = (*mac)->b_h - (h + 1);
	if (space < (*mac)->pi_w)
		return (0);
	if (fit < (*mac)->pi_h)
		return (0);
	return (1);
}

int		ft_try1(t_filler **m, int w, int h, int count)
{
	int		x;
	int		y;

	y = 0;
	while (y < (*m)->pi_h)
	{
		x = 0;
		while(x < (*m)->pi_w)
		{
			if ((*m)->b[h + y][w + x] == (*m)->op[0] || \
					(*m)->b[h + y][w + x] == (*m)->op[1])
				return (0);
			if (((*m)->b[h + y][w + x] == (*m)->pl[0] || (*m)->b[h + y]\
						[w + x] == (*m)->pl[1]) && (*m)->pi[y][x] == '*')
				count++;
			x++;
		}
		y++;
	}
	if (count != 1)
		return (0);
	ft_printf("%i %i\n", h, w);
	return (1);
}
