/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 14:46:39 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:15:53 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_print_rooms(t_room **rooms)
{
	t_room	*temp;

	temp = *rooms;
	while (temp != NULL)
	{
		if (temp->start == 1)
			ft_printf("##start\n");
		if (temp->end == 1)
			ft_printf("##end\n");
		ft_printf("%s ", temp->name);
		ft_printf("%d %d\n", temp->x, temp->y);
		temp = temp->next;
	}
}

void	ft_print_2darray(char **str)
{
	int		i;

	i = 1;
	while (str[i] != NULL)
	{
		ft_printf("%s\n", str[i]);
		i++;
	}
}

void	ft_print_roomlinks(t_room **room)
{
	t_route		*temp;
	int			i;

	temp = (*room)->edge;
	i = 1;
	while (temp != NULL)
	{
		ft_printf("link %d : %s\n", i, temp->room);
		temp = temp->next;
	}
}

void	ft_print_list(t_list *lst)
{
	t_list	*t;

	t = lst;
	while (t != NULL)
	{
		ft_printf("%s\n", (char *)t->content);
		t = t->next;
	}
}

void	ft_print_ways(t_ways **ways)
{
	t_ways	*t;
	t_path	*p;
	int		i;

	t = *ways;
	i = 1;
	while (t != NULL)
	{
		p = t->path;
		ft_printf("way %d\n", i);
		while (p != NULL)
		{
			ft_printf("%s  .. ", p->room->name);
			p = p->next;
		}
		ft_printf("\n");
		i++;
		t = t->next;
	}
}
