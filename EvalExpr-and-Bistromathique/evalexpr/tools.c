
#include "libft.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(long long int num)
{
	if (num < 0)
	{
		ft_putchar('-');
		num = num * -1;
	}
	if (num >= 10)
		ft_putnbr(num / 10);
	ft_putchar((num % 10) + 48);
}

long long int		ft_atoi(char *str)
{
	int				neg;
	long long	int		res;
	int				i;

	neg = 1;
	res = 0;
	i = 0;
	if (str[0] == '-')
	{
		neg = -1;
		i++;
	}
	while (str[i])
	{
		res = res * 10 + str[i] - '0';
		i++;
	}
	return (neg * res);
}

char	pop1()
{
	char		c;
	t_stack1	*temp;

	c = g_ops->c;
	temp = g_ops->next;
	g_ops = temp;
	return (c);
}

void	eval()
{
	long	int		a;
	long	int		b;
	char			c;

	c = pop1();
	if (c == '(')
		return ;
	if (ft_size() == 1 && (c == '+' || c == '-'))
	{
		a = pop();
		b = 0;
	}
	else if (ft_size() == 0 && (c == '+' || c == '-'))
	{
		a = 0;
		b = 0;
	}
	else if (ft_size() == 1 && (c == '*' || c == '/'))
	{
		a = pop();
		b = 1;
	}
	else if (ft_size() == 0 && (c == '*' || c == '/'))
	{
		a = 1;
		b = 1;
	}
	else
	{
		a = pop();
		b = pop();
	}
	if (c == '+')
		push(b + a);
	else if (c == '-')
		push(b - a);
	else if (c == '*')
		push(b * a);
	else if (c == '/')
		push(b / a);
	else if (c == '%')
		push(b % a);
}

int     ft_size()
{
	int         i;
	t_stack     *temp;

	temp = g_digit;
	i = 0;
	while (temp)
	{
		temp = temp->next;
		i++;
	}
	return (i);
}

