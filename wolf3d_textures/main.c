/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 16:40:02 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/21 09:13:25 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		if (ft_wolf(argv) == -1)
			ft_printf("invalid map or error\n");
	}
	else
		ft_printf("no map\n");
	return (0);
}
