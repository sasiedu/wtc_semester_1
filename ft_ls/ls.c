/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 16:47:08 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 17:38:50 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void			ft_ls(char *path)
{
	DIR						*d;
	struct dirent			*dp;
	t_list					*ls;

	ls = NULL;
	d = opendir(path);
	if (d == NULL)
	{
		ft_printf("ft_ls: %s: No such file or directory\n", path);
		return ;
	}
	dp = readdir(d);
	ft_read(&ls, dp, d);
	ls = ft_arrange(&ls);
	closedir(d);
	ft_ls_a(&ls);
	ft_print_ls(ls);
	ft_memdel((void **)&ls);
}

void			ft_ls2(int argc, char **argv)
{
	int			i;

	i = argc - 1;
	while (i > 0)
	{
		if (argc > 2)
			ft_printf("%s:\n", argv[i]);
		ft_ls(argv[i]);
		i--;
	}
}

void			ft_read(t_list **ls, struct dirent *dp, DIR *d)
{
	*ls = (t_list *)malloc(sizeof(t_list));
	(*ls)->content = ft_memalloc(ft_strlen(dp->d_name) + 1);
	ft_strncpy((*ls)->content, dp->d_name, ft_strlen(dp->d_name));
	(*ls)->next = NULL;
	if ((dp = readdir(d)) != NULL)
		ft_read(&(*ls)->next, dp, d);
}
