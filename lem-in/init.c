/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 13:38:08 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:32:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_lemin		*ft_init_map(void)
{
	t_lemin		*lem;
	char		*line;

	if ((lem = (t_lemin *)malloc(sizeof(t_lemin))) == NULL)
		ft_error(1, "lem");;
	line = NULL;
	lem->num_paths = 0;
	lem->num_rooms = 0;
	lem->num_ants = 0;
	lem->ants = NULL;
	lem->ways = NULL;
	lem->rooms = NULL;
	lem->start = NULL;
	lem->end = NULL;
	lem->links = NULL;
	lem->links = ft_add_2darray(lem->links, "pop");
	if (get_next_line(0, &line) == 1)
	{
		if (ft_check_ants(line) == 0)
			ft_error(2, NULL);
		ft_init_ants(&lem, ft_atoi(line));
	}
	ft_init_rooms(&lem);
	return (lem);
}

void		ft_init_rooms(t_lemin **lem)
{
	char	*line;
	t_room	*room;

	line = NULL;
	while (get_next_line(0, &line) == 1 && ft_strchr(line, '-') == NULL)
	{
		if (ft_strcmp(line, "##start") == 0 || ft_strcmp(line, "##end") == 0)
			ft_init_start_end(&(*lem), line);
		else
		{
			room = build_room(line, 0, 0, 0);
			ft_add_room(&(*lem)->rooms, room);
		}
	}
	if ((*lem)->rooms == NULL)
		ft_error(3, "rooms");
	ft_init_links(&(*lem), line);
	while (get_next_line(0, &line) == 1 && ft_strchr(line, '-') != NULL)
		ft_init_links(&(*lem), line);
}

void		ft_init_links(t_lemin **lem, char *line)
{
	char	**s;
	t_room	*room1;
	t_room	*room2;

	s = ft_strsplit(line, '-');
	room1 = ft_find_room(&(*lem)->rooms, s[0]);
	room2 = ft_find_room(&(*lem)->rooms, s[1]);
	ft_add_roomlink(&room1->edge, s[1]);
	ft_add_roomlink(&room2->edge, s[0]);
	(*lem)->links = ft_add_2darray((*lem)->links, line);
	ft_free_2darray(s);
}
