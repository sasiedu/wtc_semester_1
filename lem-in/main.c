/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 15:01:58 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:32:47 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		main(void)
{
	t_lemin		*lem;
	t_list		*lst;
	char		*str;

	lem = NULL;
	lst = NULL;
	str = NULL;
	lem = ft_init_map();
	ft_check_rooms(&lem);
	str = ft_strjoin(lem->start->name, "");
	ft_find_path(&lem, &lem->start, &lst, str);
	ft_check_path(&lst);
	lem->ways = NULL;
	ft_build_ways(&lem, &lem->ways, &lst);
	ft_check_ways(&lem);
	ft_printf("%d\n", lem->num_ants);
	ft_print_rooms(&lem->rooms);
	ft_print_2darray(lem->links);
	ft_printf("\n");
	ft_moving_ants(&lem);
	return (0);
}
