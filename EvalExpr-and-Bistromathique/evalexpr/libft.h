

#ifndef	LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>

typedef	struct	s_stack
{
	long	long	int			x;
	struct s_stack		*next;
}				t_stack;

typedef	struct	s_stack1
{
	char				c;
	struct s_stack1		*next;
}				t_stack1;

t_stack			*g_digit;
t_stack1		*g_ops;
int				g_i;

void			eval_sub();
void			eval_add();
void			eval_mul();
void			eval_mod();
void			eval_div();
int				ft_size();
void			ft_putchar(char c);
void			ft_putnbr(long long int num);
long	long	int				ft_atoi(char *str);
char			pop1();
void			eval();
void			push(long long int x);
void			push1(char c);
t_stack			*ft_create(long long int x);
t_stack1		*ft_create1(char c);
long	long int				pop();
char			*get_num(char *str);
void			eval_b();
void			eval_ops(char c);
int				eval_expr(char *str);

#endif
