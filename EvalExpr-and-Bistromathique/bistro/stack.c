
#include "libft.h"

void		push(int x)
{
	t_stack		*temp;

	temp = g_digit;
	g_digit = ft_create(x);
	g_digit->next = temp;
}

void		push1(char c)
{
	t_stack1	*temp;

	temp = g_ops;
	g_ops = ft_create1(c);
	g_ops->next = temp;
}

t_stack		*ft_create(int x)
{
	t_stack		*temp;

	temp = (t_stack*)malloc(sizeof(t_stack));
	if (temp)
	{
		temp->x = x;
		temp->next = NULL;
	}
	return (temp);
}

t_stack1	*ft_create1(char c)
{
	t_stack1	*temp;

	temp = (t_stack1*)malloc(sizeof(t_stack1));
	if (temp)
	{
		temp->c = c;
		temp->next = NULL;
	}
	return (temp);
}

int			pop()
{
	int			x;
	t_stack		*temp;

	x = g_digit->x;
	temp = g_digit->next;
	g_digit = temp;
	return (x);
}
