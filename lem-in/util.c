/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 10:52:33 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:17:33 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

size_t		ft_strsplit_size(char **str)
{
	size_t	i;

	i = 0;
	if (str == NULL)
		return (0);
	while (str[i])
		i++;
	return (i);
}

char		**ft_add_2darray(char **save, char *add)
{
	int		size;
	int		i;
	char	**temp;

	size = ft_strsplit_size(save);
	if ((temp = (char **)malloc(sizeof(char *) * size + 2)) == NULL)
		ft_error(1, "temp");
	i = 0;
	while (i < size)
	{
		temp[i] = ft_strjoin(save[i], "");
		i++;
	}
	temp[i] = ft_strjoin(add, "");
	temp[i + 1] = NULL;
	return (temp);
}

void		ft_build_ways(t_lemin **lem, t_ways **ways, t_list **lst)
{
	t_list	*t;

	t = *lst;
	while (t != NULL)
	{
		ft_add_ways(&(*lem), &(*ways), (char *)t->content);
		t = t->next;
	}
}

void		ft_add_ways(t_lemin **lem, t_ways **ways, char *str)
{
	char	**s;
	int		i;

	if (*ways != NULL)
		return (ft_add_ways(&(*lem), &(*ways)->next, str));
	if ((*ways = (t_ways *)malloc(sizeof(t_ways))) == NULL)
		ft_error(1, "ways");
	s = ft_strsplit(str, ' ');
	i = 0;
	(*ways)->path = NULL;
	while (s[i] != NULL)
	{
		ft_add_rooms_path(&(*lem), &(*ways)->path, s[i]);
		i++;
	}
	(*ways)->next = NULL;
}

void		ft_add_rooms_path(t_lemin **lem, t_path **path, char *str)
{
	if (*path != NULL)
		return (ft_add_rooms_path(&(*lem), &(*path)->next, str));
	if ((*path = (t_path *)malloc(sizeof(t_path))) == NULL)
		ft_error(1, "path");
	(*path)->room = ft_find_room(&(*lem)->rooms, str);
	(*path)->next = NULL;
}
