#include "libft.h"
t_list		*ft_up(t_list *elem);
void		ft_lstprint(t_list **alst);

int		main(int argc, char **argv)
{
	t_list		*new;
	t_list		*old;
	t_list		*add;
	t_list		*add1;

	if (argc > 1)
	{
		new = ft_lstnew(argv[1], ft_atoi(argv[2]));
		add = ft_lstnew(argv[3], ft_atoi(argv[4]));
		add1 = ft_lstnew(argv[5], ft_atoi(argv[6]));
		ft_lstadd(&new, add);
		ft_lstadd(&new, add1);
		ft_lstprint(&new);
		old = ft_lstmap(new, &ft_up);
		ft_lstprint(&old);
	}
	return (0);
}
