/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   down.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/16 11:58:43 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 14:02:27 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			move_down(t_filler **m, int h)
{
	int			w;

	while (h < (*m)->b_h - (*m)->pi_h)
	{
		w = 0;
		while (w < (*m)->b_w - (*m)->pi_w)
		{
			if (ft_try1(&(*m), w, h, 0) == 1)
				return (1);
			w++;
		}
		h++;
	}
	return (0);
}

int			move_up(t_filler **m)
{
	int			w;
	int			h;

	h = ((*m)->b_h - (*m)->pi_h) - 1;
	while (h >= 0)
	{
		w = ((*m)->b_w - (*m)->pi_w) - 1;
		while (w >= 0)
		{
			if (ft_try1(&(*m), w, h, 0) == 1)
				return (1);
			w--;
		}
		h--;
	}
	return (0);
}
