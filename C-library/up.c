/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   up.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/10 16:55:01 by sasiedu           #+#    #+#             */
/*   Updated: 2016/05/12 09:05:58 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_upper(char *str)
{
	int		i;
	char	*s1;

	i = 0;
	s1 = ft_strnew(10);
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			s1[i] = str[i] - 32;
		else
			s1[i] = str[i];
		i++;
	}
	return (s1);
}

t_list		*ft_up(t_list *elem)
{
	t_list		*temp;

	temp = (t_list *)malloc(sizeof(t_list));
	ft_putendl("a");
	temp->content = ft_upper(elem->content);
	temp->content_size = elem->content_size;
	ft_putendl("b");
	return (temp);
}
