/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   order.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 10:47:21 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:16:55 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

t_list	*ft_arrange(t_list **ls)
{
	t_list		*temp;
	int			i;
	int			size;

	i = 0;
	temp = NULL;
	size = ft_list_size(*ls);
	while (i < size)
	{
		ft_compare_name(&temp, &(*ls));
		free((*ls)->content);
		*ls = (*ls)->next;
		i++;
	}
	return (temp);
}

void	ft_compare_name(t_list **temp, t_list **ls)
{
	t_list		*t;

	t = NULL;
	if (*temp == NULL)
	{
		*temp = (t_list *)malloc(sizeof(t_list));
		(*temp)->content = ft_memalloc(ft_strlen((*ls)->content) + 1);
		if ((*temp)->content == NULL)
			exit(-1);
		ft_strncpy((*temp)->content, (*ls)->content, ft_strlen((*ls)->content));
		(*temp)->next = NULL;
		return ;
	}
	if (ft_strcmp((*temp)->content, (*ls)->content) > 1)
	{
		t = (t_list *)malloc(sizeof(t_list));
		t->content = ft_memalloc(ft_strlen((*ls)->content) + 1);
		ft_strncpy(t->content, (*ls)->content, ft_strlen((*ls)->content));
		t->next = *temp;
		*temp = t;
		t = NULL;
	}
	else
		ft_compare_name(&(*temp)->next, &(*ls));
}

void	ft_read_list(t_list **lst, struct dirent *dp, DIR *d)
{
	*lst = (t_list *)malloc(sizeof(t_list));
	(*lst)->content = ft_strjoin(dp->d_name, "");
	(*lst)->next = NULL;
	if ((dp = readdir(d)) != NULL)
		ft_read_list(&(*lst)->next, dp, d);
}

void	ft_print_curr(t_list *lst, int fd)
{
	int		count;
	char	*str;
	t_list	*t;

	t = lst;
	count = 0;
	while (t != NULL)
	{
		if (count > 0)
			write(fd, " ", 1);
		str = (char *)t->content;
		if (t->content != NULL && str[0] != '.')
		{
			write(fd, t->content, ft_strlen(t->content));
			count++;
		}
		t = t->next;
	}
}

int		ft_list_size(t_list *ls)
{
	int			size;
	t_list		*temp;

	size = 0;
	temp = ls;
	while (temp != NULL)
	{
		size++;
		temp = temp->next;
	}
	return (size);
}
