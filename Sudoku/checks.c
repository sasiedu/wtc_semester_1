/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/09 23:02:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/09 23:56:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

int	ft_check_row(int row, int board[][9], int num)
{
	int	col;
	
	col = 0;
	while (col < 9)
	{
		if (board[col][row] == num)
			return (0);
		col++;
	}
	return (1);
}

int	ft_check_col(int col, int board[][9], int num)
{
	int	row;

	row = 0;
	while (row < 9)
	{
		if (board[col][row] == num)
			return (0);
		row++;
	}
	return (1);
}

int	ft_check_small_box(int row, int col, int board[][9], int num)
{
	int	r;
	int	c;

	row = (row / 3) * 3;
	col = (col / 3) * 3;
	c = 0;
	while (c < 3)
	{
		r = 0;
		while (r < 3)
		{
			if (board[col + c][row + r] == num)
				return (0);
			r++;
		}
		c++;
	}
	return (1);
}
