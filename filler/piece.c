/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   piece.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 23:19:41 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 14:08:03 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_build_piece(t_filler **mac)
{
	char		**split;

	split = ft_strsplit((*mac)->line, ' ');
	(*mac)->pi_h = ft_atoi(split[1]);
	(*mac)->pi_w = ft_atoi(split[2]);
	ft_fill_p(&(*mac));
	ft_start(&(*mac), 0);
	if ((*mac)->start == 1)
		ft_play(&(*mac));
	else
		ft_play2(&(*mac));
}

void		ft_fill_p(t_filler **mac)
{
	int			h;

	h = 0;
	(*mac)->pi = (char **)malloc(sizeof(char *) * (*mac)->pi_h);
	while (h < (*mac)->pi_h)
	{
		get_next_line(0, &(*mac)->line);
		(*mac)->pi[h] = ft_strnew(ft_strlen((*mac)->line));
		ft_strncpy((*mac)->pi[h], (*mac)->line, ft_strlen((*mac)->line));
		h++;
	}
}

void		ft_start(t_filler **mac, int h)
{
	int		w;

	if ((*mac)->start == 2)
	{
		ft_start2(&(*mac));
		return ;
	}
	while (h < (*mac)->pi_h)
	{
		w = 0;
		while (w < (*mac)->pi_w)
		{
			if ((*mac)->pi[h][w] == '*')
			{
				(*mac)->pi_sw = w;
				(*mac)->pi_sh = h;
				return ;
			}
			w++;
		}
		h++;
	}
}

void		ft_start2(t_filler **mac)
{
	int		h;
	int		w;

	h = (*mac)->pi_h - 1;
	while (h >= 0)
	{
		w = (*mac)->pi_w - 1;
		while (w >= 0)
		{
			if ((*mac)->pi[h][w] == '*')
			{
				(*mac)->pi_sw = w;
				(*mac)->pi_sh = h;
				return ;
			}
			w--;
		}
		h--;
	}
}
