

#ifndef	LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/uio.h>

# define SYNTAXE_ERROR_MSG	"syntax error\n"
# define size_t	int

typedef	struct	s_stack
{
	unsigned int		x;
	struct s_stack		*next;
}				t_stack;

typedef	struct	s_stack1
{
	char				c;
	struct s_stack1		*next;
}				t_stack1;

t_stack			*g_digit;
t_stack1		*g_ops;
int				g_i;

void					eval_sub();
void					eval_add();
void					eval_mul();
void					eval_mod();
void					eval_div();
void					ft_putchar(char c);
char					pop1();
void					eval();
void					push(int x);
void					push1(char c);
t_stack					*ft_create(int x);
t_stack1				*ft_create1(char c);
int						pop();
void					eval_b();
void					eval_ops(char c);
int						eval_expr(char *base, char *ops, char *expr);
char					*get_num(char *base, char *expr);
int						ft_find(char *str, char c);
size_t					ft_strlen(char *strlen);
unsigned	int			ft_uatoi(char *nbr);
int						ft_fill_buff(char *buf, size_t size);
int						ft_check_args_n_malloc_buf(int argc, char **argv, char **buf);
int						ft_size();

#endif
