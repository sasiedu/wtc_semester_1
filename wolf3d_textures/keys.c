/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 16:11:42 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/24 10:49:05 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_wolf	*key_up(t_wolf *e)
{
	double		i;

	i = 0.2 * 0.3;
	if (e->world[(int)(e->posy + e->diry * i)][(int)e->posx] == 0)
		e->posy += e->diry * i;
	if (e->world[(int)e->posy][(int)(e->posx + e->dirx * i)] == 0)
		e->posx += e->dirx * i;
	return (e);
}

t_wolf	*key_down(t_wolf *e)
{
	double		i;

	i = 0.2 * 0.3;
	if (e->world[(int)(e->posy - e->diry * i)][(int)e->posx] == 0)
		e->posy -= e->diry * i;
	if (e->world[(int)e->posy][(int)(e->posx - e->dirx * i)] == 0)
		e->posx -= e->dirx * i;
	return (e);
}

t_wolf	*key_left(t_wolf *e)
{
	double		i;

	i = 0.2 * 0.3;
	if (e->world[(int)(e->posy - e->dirx * i)][(int)e->posx] == 0)
		e->posy -= e->dirx * i;
	if (e->world[(int)e->posy][(int)(e->posx - e->diry * i)] == 0)
		e->posx -= e->diry * i;
	return (e);
}

t_wolf	*key_right(t_wolf *e)
{
	double		i;

	i = 0.2 * 0.3;
	if (e->world[(int)(e->posy + e->dirx * i)][(int)e->posx] == 0)
		e->posy += e->dirx * i;
	if (e->world[(int)e->posy][(int)(e->posx + e->diry * i)] == 0)
		e->posx += e->diry * i;
	return (e);
}
