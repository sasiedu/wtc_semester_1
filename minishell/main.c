/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/06 18:34:16 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 12:48:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

int		ft_minishell(char **av, char **environ)
{
	t_shell		*ev;

	if ((ev = (t_shell *)malloc(sizeof(t_shell))) == NULL)
		return (-1);
	ev->olddir = av[0];
	write(1, "$hell> ", 7);
	ev->hold = ft_lstnew((void *)environ[0], ft_strlen(environ[0]));
	ft_load(&ev, environ, &ev->hold);
	while (get_next_line(0, &ev->line) == 1)
	{
		if (ft_strlen(ev->line) > 0)
			ft_exec(&ev, environ);
		ev->oldline = ft_strjoin(ev->line, "");
		write(1, "$hell> ", 7);
	}
	ft_lstdel(&ev->hold, &ft_del);
	ft_memdel((void **)&ev);
	return (0);
}

int		main(int ac, char **av, char **environ)
{
	int		ret;

	if (ac != 1)
		return (-1);
	ret = ft_minishell(av, environ);
	if (ret == -1)
		write(1, "Malloc error. Memory allocation failed\n", 39);
	return (0);
}
