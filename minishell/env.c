/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 14:08:23 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/08 10:26:10 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_setenv(t_shell **ev, char **split)
{
	int		ret;
	int		i;

	i = 0;
	while (split[i])
		i++;
	if (i != 3)
	{
		ft_printf("error : setenv: error: too many arguments to function");
		ft_printf(" call, expected 2, have %d\n", ft_abs(i - 1));
		return ;
	}
	ret = ft_update(&(*ev)->hold, split);
	if (ret == 0)
		ft_lstaddend(&(*ev)->hold, ft_lstnew(ft_strjoin(\
						ft_strjoin(split[1], "="), split[2]), 0));
	else
		ft_setenv2(&(*ev), split, &(*ev)->hold, 1);
}

void	ft_setenv2(t_shell **ev, char **split, t_list **hold, int set)
{
	char	**new;
	char	*temp;

	new = ft_strsplit((*hold)->content, '=');
	if (ft_strcmp(new[0], split[1]) != 0)
		return (ft_setenv2(&(*ev), split, &(*hold)->next, set));
	if (set == 1)
		temp = ft_strjoin(ft_strjoin(new[0], "="), split[2]);
	if (ft_strcmp("OLDPWD", new[0]) == 0)
		ft_strdel(&(*ev)->olddir);
	else if (ft_strcmp("PWD", new[0]) == 0)
		ft_strdel(&(*ev)->currdir);
	else if (ft_strcmp("HOME", new[0]) == 0)
		ft_strdel(&(*ev)->home);
	else if (ft_strcmp("USER", new[0]) == 0)
		ft_strdel(&(*ev)->user);
	else
		ft_memdel(&(*hold)->content);
	if (set == 1)
		ft_setenv3(&temp, &(*hold), &(*ev), new);
	else if (set == 0)
		ft_memdel(&(*hold)->content);
}

void	ft_setenv3(char **temp, t_list **hold, t_shell **ev, char **new)
{
	if (ft_strcmp("OLDPWD", new[0]) == 0)
		(*ev)->olddir = *temp;
	else if (ft_strcmp("PWD", new[0]) == 0)
		(*ev)->currdir = *temp;
	else if (ft_strcmp("HOME", new[0]) == 0)
		(*ev)->home = *temp;
	else if (ft_strcmp("USER", new[0]) == 0)
		(*ev)->user = *temp;
	else
		(*hold)->content = *temp;
}

int		ft_update(t_list **hold, char **split)
{
	t_list	*temp;
	char	**new;

	temp = *hold;
	while (temp != NULL)
	{
		new = ft_strsplit(temp->content, '=');
		if (ft_strcmp(new[0], split[1]) == 0)
			return (1);
		temp = temp->next;
	}
	return (0);
}
