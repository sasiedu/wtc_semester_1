/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 16:44:19 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/23 17:05:06 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		ft_wolf(char **argv)
{
	t_wolf		*env;

	env = (t_wolf *)malloc(sizeof(t_wolf));
	if (ft_check_world(argv, &env) == -1)
		return (-1);
	ft_build_map(argv, &env, env->h, env->w);
	env->mlx = mlx_init();
	env->win = mlx_new_window(env->mlx, WIN_W, WIN_H, "WOLF3D");
	env->img = mlx_new_image(env->mlx, WIN_W, WIN_H);
	env = ft_init(env, env->h, env->w);
	ft_printf("posx : %i  dirx : %i\n", env->posx, env->dirx);
	ft_printf("posy : %i  diry : %i\n", env->posy, env->diry);
	mlx_hook(env->win, 2, KEYPRESSMASK, key_hook, env);
	mlx_expose_hook(env->win, expose_hook, env);
	mlx_loop(env->mlx);
	return (0);
}
