/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/06 18:37:16 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:54:44 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHELL_H
# define SHELL_H

# include "libft.h"
# include "ft_printf.h"
# include "get_next_line.h"
# include <dirent.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <fcntl.h>

typedef	struct		s_shell
{
	char			*line;
	char			*oldline;
	char			*olddir;
	char			*currdir;
	char			*bin;
	char			*user;
	char			*home;
	char			*temp;
	char			*end;
	t_list			*hold;
}					t_shell;

typedef	struct		s_echo
{
	int				n;
	int				ee;
	int				e;
	int				fd;
	int				wr;
	int				app;
}					t_echo;

t_list				*ft_arrange(t_list **ls);
int					ft_minishell(char **av, char **environ);
int					ft_check_dir(char *path, t_shell *env, char **temp);
int					ft_update(t_list **hold, char **split);
int					ft_size_double(char **str);
int					ft_process_e(int fd, char *str);
int					ft_init_fd(t_echo *inf, char **split);
int					ft_all_curr(t_shell *ev, char *str, t_echo *inf);
int					ft_list_size(t_list *ls);
int					ft_check_echo2(char **split);
char				*ft_get_path(char *path, t_shell *env, char *temp);
char				*ft_chr(char *str, char c);
void				ft_load(t_shell **ev, char **env, t_list **hold);
void				ft_exec(t_shell **ev, char **env);
void				ft_do_cd(t_shell **ev, char **split);
void				ft_do_cd2(t_shell **ev, char **split);
void				ft_bin(t_shell **ev, char **split, char **env);
void				ft_do_bin(t_shell **ev, char **split, char **env);
void				ft_print_env(t_shell *ev);
void				ft_setenv(t_shell **ev, char **split);
void				ft_setenv2(t_shell **ev, char **split, t_list **hold, \
		int set);
void				ft_setenv3(char **temp, t_list **hold, t_shell **ev, \
		char **new);
void				ft_unsetenv(t_shell **ev, char **split);
void				ft_free_double(char **str);
void				ft_get_flags(t_echo **inf, char *str);
void				ft_get_flags2(t_echo **inf, char *str);
void				ft_init_echo(t_echo **inf);
void				ft_check_echo(t_echo **inf, char **split);
void				ft_do_echo(t_shell **ev, char **split);
void				ft_print_echo(t_shell *ev, char *str, t_echo *inf, \
		char **split);
void				ft_echo_flags_print(t_shell *ev, char *str, t_echo *inf, \
		char **split);
void				ft_print_echo2(t_shell *ev, char *str, t_echo *inf, int fd);
void				ft_process_echo3(t_shell *ev, char *str, t_echo *inf);
void				ft_compare_name(t_list **temp, t_list **ls);
void				ft_print_curr(t_list *lst, int fd);
void				ft_read_list(t_list **lst, struct dirent *dp, DIR *d);
void				ft_print_end(t_list *ls, char *str, int fd);
void				ft_print_frnt(t_list *ls, char *str, int fd);
void				ft_pwd(t_shell *ev, char **split);

#endif
