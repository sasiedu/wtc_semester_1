/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_l.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 23:24:17 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/18 11:21:13 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		ft_print_l(t_list *ls, char *str)
{
	t_info		*file;
	t_list		*temp;
	int			size;
	int			i;

	size = ft_list_size(ls);
	i = 0;
	temp = ls;
	ft_l_size(ls, str);
	while (i < size)
	{
		file = (t_info *)malloc(sizeof(t_info));
		file->clock = (t_time *)malloc(sizeof(t_time));
		if (ft_get_info(&file, &temp, str) == 0)
		{
			ft_print_info(&file);
			ft_memdel((void **)&file);
		}
		temp = temp->next;
		i++;
	}
}

void		ft_print_info(t_info **f)
{
	ft_printf("%c%s%s%s", (*f)->chmod->d, (*f)->chmod->usr, (*f)->chmod->grp, \
			(*f)->chmod->other);
	ft_printf("%6d %-10s %-13s ", (*f)->n_links, (*f)->pw_uid, (*f)->gw_gid);
	ft_printf("%6d %3s %02d ", (*f)->file_size, (*f)->clock->mon, \
			(*f)->clock->date);
	ft_printf("%02d:%02d %s\n", (*f)->clock->hh, (*f)->clock->mm, \
			(*f)->name);
}

void		ft_l_size(t_list *ls, char *path)
{
	int				size;
	struct stat		*buf;
	t_list			*temp;

	buf = (struct stat *)malloc(sizeof(struct stat));
	temp = ls;
	size = 0;
	while (temp != NULL)
	{
		if (lstat(ft_get_path(path, temp->content), buf) != -1)
			size = size + buf->st_blocks;
		temp = temp->next;
	}
	ft_printf("total %i\n", size);
	ft_memdel((void **)&buf);
}
