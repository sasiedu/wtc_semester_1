/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   info.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 22:41:19 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/15 22:48:11 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_player_info(t_filler **mac)
{
	if ((*mac)->line[10] == '1')
		ft_player_one(&(*mac));
	else
		ft_player_two(&(*mac));
}

void		ft_player_one(t_filler **mac)
{
	(*mac)->pl_n = 1;
	(*mac)->pl[0] = 'o';
	(*mac)->pl[1] = 'O';
	(*mac)->op[0] = 'x';
	(*mac)->op[1] = 'X';
}

void		ft_player_two(t_filler **mac)
{
	(*mac)->pl_n = 2;
	(*mac)->pl[0] = 'x';
	(*mac)->pl[1] = 'X';
	(*mac)->op[0] = 'o';
	(*mac)->op[1] = 'O';
}
