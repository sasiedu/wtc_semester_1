/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grid.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 22:48:35 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 11:30:28 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_build_grid(t_filler **mac)
{
	char		**split;

	split = ft_strsplit((*mac)->line, ' ');
	(*mac)->b_h = ft_atoi(split[1]);
	(*mac)->b_w = ft_atoi(split[2]);
	get_next_line(0, &(*mac)->line);
	get_next_line(0, &(*mac)->line);
	ft_fill(&(*mac));
}

void		ft_fill(t_filler **mac)
{
	int			h;
	char		**split;

	(*mac)->b = (char **)malloc(sizeof(char *) * (*mac)->b_h);
	h = 0;
	while (h < (*mac)->b_h)
	{
		split = ft_strsplit((*mac)->line, ' ');
		(*mac)->b[h] = ft_strnew(ft_strlen(split[1]));
		ft_strncpy((*mac)->b[h], split[1], ft_strlen(split[1]));
		if ((*mac)->start == 0)
			ft_check(split[1], &(*mac), h);
		get_next_line(0, &(*mac)->line);
		h++;
	}
}

void		ft_check(char *str, t_filler **mac, int h)
{
	int			i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == (*mac)->pl[0] || str[i] == (*mac)->pl[1])
		{
			if (h < ((*mac)->b_h / 2))
				(*mac)->start = 1;
			else
				(*mac)->start = 2;
			return ;
		}
		i++;
	}
}
