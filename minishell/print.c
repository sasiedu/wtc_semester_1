/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 10:35:26 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/08 11:11:48 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_print_env(t_shell *ev)
{
	t_list		*temp;
	int			i;

	temp = ev->hold;
	i = 0;
	while (temp != NULL)
	{
		if (temp->content != NULL)
		{
			if (ft_strncmp("PWD=", (char *)temp->content, 4) == 0)
				ft_printf("%s\n", ev->currdir);
			else if (ft_strncmp("OLDPWD=", (char *)temp->content, 7) == 0)
				ft_printf("%s\n", ev->olddir);
			else if (ft_strncmp("HOME=", (char *)temp->content, 5) == 0)
				ft_printf("%s\n", ev->home);
			else if (ft_strncmp("USER=", (char *)temp->content, 5) == 0)
				ft_printf("%s\n", ev->user);
			else if (ft_strncmp("_=", (char *)temp->content, 2) == 0)
				i++;
			else
				ft_printf("%s\n", (char *)temp->content);
		}
		temp = temp->next;
	}
	ft_printf("%s\n", ev->end);
}
