/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 15:29:23 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:15:38 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_check_path(t_list **lst)
{
	t_list	*t;

	t = *lst;
	while (t != NULL)
	{
		ft_compare_path(&(*lst)->next, (char *)t->content);
		t = t->next;
	}
}

void	ft_compare_path(t_list **lst, char *path)
{
	t_list	*t;

	t = *lst;
	while (t != NULL)
	{
		if (ft_compare_name((char *)t->content, path) == 1)
		{
			if (ft_strlen((char *)t->content) > ft_strlen(path))
				ft_list_remove_if(&(*lst), t->content, ft_strcmp);
			else
				ft_list_remove_if(&(*lst), path, ft_strcmp);
		}
		t = t->next;
	}
}

int		ft_compare_name(const char *dest, const char *src)
{
	int		i;
	int		size;
	int		size2;
	char	**s1;
	char	**s2;

	if (ft_strcmp(dest, src) == 0)
		return (0);
	s1 = ft_strsplit(dest, ' ');
	s2 = ft_strsplit(src, ' ');
	size = ft_strsplit_size(s1) - 1;
	size2 = ft_strsplit_size(s2) - 1;
	i = 0;
	while (s1[i] && s2[i])
	{
		if (i != 0 && i != size && i != size2)
		{
			if (ft_strcmp(s1[i], s2[i]) == 0)
				return (1);
		}
		i++;
	}
	return (0);
}
