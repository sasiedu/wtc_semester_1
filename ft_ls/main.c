/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 11:42:53 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 17:31:46 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int		main(int argc, char **argv)
{
	if (argc == 1)
		ft_ls(".");
	if (argc > 1 && argv[1][0] != '-')
		ft_ls2(argc, argv);
	if (argc == 2 && argv[1][0] == '-')
		ft_ls_flags(argv, ".");
	if (argc > 2 && argv[1][0] == '-')
		ft_ls_flags2(argc, argv);
	return (0);
}
