/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 14:13:40 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:14:50 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_init_start_end(t_lemin **lem, char *prev)
{
	char	*line;
	t_room	*room;

	line = NULL;
	if (get_next_line(0, &line) == 0)
		return ;
	if (ft_strcmp(prev, "##start") == 0)
	{
		room = build_room(line, 1, 0, (*lem)->num_ants);
		(*lem)->start = room;
		ft_add_room(&(*lem)->rooms, room);
	}
	else if (ft_strcmp(prev, "##end") == 0)
	{
		room = build_room(line, 0, 1, 0);
		(*lem)->end = room;
		ft_add_room(&(*lem)->rooms, room);
	}
}
