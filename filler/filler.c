/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 22:22:38 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 14:04:43 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			main(void)
{
	t_filler	*mac;

	mac = (t_filler *)malloc(sizeof(t_filler));
	mac->start = 0;
	while (get_next_line(0, &(mac)->line) == 1)
	{
		if (ft_strncmp("$$$ exec p", mac->line, 9) == 0)
			ft_player_info(&mac);
		if (ft_strncmp("Plateau", mac->line, 7) == 0)
			ft_build_grid(&mac);
		if (ft_strncmp("Piece", mac->line, 4) == 0)
			ft_build_piece(&mac);
	}
	return (0);
}
