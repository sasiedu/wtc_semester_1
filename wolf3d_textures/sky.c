/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sky.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 15:27:55 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/22 15:56:31 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_sky(t_wolf **e, int w, t_img *d)
{
	int		h;
	int		end;
	int		i;
	int		c;

	h = 0;
	end = (*e)->start;
	while (h < end)
	{
		c = 135206250;
		i = (w * 4) + (WIN_W * h * d->bps / 8);
		d->data[i + 2] = c;
		d->data[i + 1] = c;
		d->data[i] = c;
		h++;
	}
}

void	ft_ground(t_wolf **e, int w, t_img *d)
{
	int		h;
	int		i;
	int		c;

	h = (*e)->start + (*e)->wall;
	while (h < WIN_H)
	{
		c = 135206250;
		i = (w * 4) + (WIN_W * h * d->bps / 8);
		d->data[i + 2] = c;
		d->data[i + 1] = c >> 8;
		d->data[i] = c >> 16;
		h++;
	}
}
