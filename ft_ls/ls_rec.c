/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_rec.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/14 01:29:00 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 17:18:37 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		ft_search_dir(char **argv, char *path, t_list **ls)
{
	int					size;
	int					i;
	struct stat			*buf;
	char				*temp;

	if ((buf = (struct stat *)malloc(sizeof(struct stat))) == NULL)
		exit(-1);
	i = 0;
	size = ft_list_size(*ls);
	while (i < size)
	{
		if (ls_rec_search((*ls)->content) == 1 && lstat(ft_get_path(path, \
							(*ls)->content), buf) != -1)
		{
			temp = ft_uni_convert(buf->st_mode, 8);
			if ((ft_dir(ft_atoi(ft_strsub(temp, 0, ft_strlen(temp) - 4)))) \
					== 'd')
				ft_dir_ent(path, &(*ls), argv);
			temp = NULL;
		}
		i++;
		(*ls) = (*ls)->next;
	}
	ft_memdel((void **)&buf);
}

void		ft_dir_ent(char *path, t_list **ls, char **argv)
{
	char	*str;

	str = ft_get_path(path, (*ls)->content);
	ft_printf("\n");
	ft_ls_flags(argv, ft_get_path(path, (*ls)->content));
	ft_memdel((void **)&str);
}
