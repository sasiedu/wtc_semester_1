/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstprint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/12 08:27:11 by sasiedu           #+#    #+#             */
/*   Updated: 2016/05/12 08:54:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstprint(t_list **alst)
{
	t_list		*temp;

	temp = *alst;
	while (&(*temp))
	{
		ft_putendl(temp->content);
		ft_putnbr(temp->content_size);
		ft_putchar('\n');
		temp = temp->next;
	}
}
