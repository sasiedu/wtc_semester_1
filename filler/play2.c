/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/16 13:13:33 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 14:06:20 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		ft_play2(t_filler **mac)
{
	static	int		count = 1;
	int				i;

	i = 0;
	if (count == 2)
	{
		i = loop_up(&(*mac));
		if (i == 0)
			i = move_down(&(*mac), 0);
		if (i == 0)
			ft_printf("0 0\n");
		count = 1;
		return;
	}
	if (count == 1)
	{
		i = move_down(&(*mac), 0);
		if (i == 0)
			i = loop_up(&(*mac));
		if (i == 0)
			ft_printf("0 0\n");
		count = 2;
	}
}
