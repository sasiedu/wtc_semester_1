/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 17:01:50 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 17:09:11 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void			get_time(char *name, t_time **t, char *path)
{
	time_t			*clock;
	struct stat		*buf;
	char			*time;
	char			**str;

	buf = (struct stat *)malloc(sizeof(struct stat));
	lstat(ft_get_path(path, name), buf);
	clock = &buf->st_mtimespec.tv_sec;
	time = ctime(clock);
	str = ft_strsplit(time, ' ');
	(*t)->mon = ft_strjoin("", str[1]);
	(*t)->date = ft_atoi(str[2]);
	(*t)->yy = ft_atoi(str[4]);
	(*t)->hh = ft_atoi(&str[3][0]);
	(*t)->mm = ft_atoi(&str[3][3]);
	(*t)->ss = ft_atoi(&str[3][6]);
	ft_memdel((void *)&buf);
	ft_memdel((void *)str);
	time = NULL;
	str = NULL;
}

char			*ft_get_path(char *path, char *name)
{
	if (path[0] == '.')
		return (name);
	if (path[ft_strlen(path) - 1] != '/' && name[0] != '/')
	{
		path = ft_strjoin(path, "/");
		return (ft_strjoin(path, name));
	}
	if (path[ft_strlen(path) - 1] != '/' && name[0] == '/')
		return (ft_strjoin(path, name));
	if (path[ft_strlen(path) - 1] == '/' && name[0] != '/')
		return (ft_strjoin(path, name));
	if (path[ft_strlen(path) - 1] == '/' && name[0] == '/')
		return (ft_strjoin(path, &name[1]));
	return (NULL);
}
