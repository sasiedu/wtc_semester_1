/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expose.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 11:22:24 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/24 10:37:25 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		expose_hook(t_wolf *env)
{
	mlx_destroy_image(env->mlx, env->img);
	env->img = mlx_new_image(env->mlx, WIN_W, WIN_H);
	env = ft_cast(env);
	mlx_put_image_to_window(env->mlx, env->win, env->img, 0, 0);
	return (0);
}

t_wolf	*ft_cast(t_wolf *env)
{
	t_cast		*r;
	t_img		*d;
	t_img		*p;
	int			w;

	w = 0;
	r = (t_cast *)malloc(sizeof(t_cast));
	d = (t_img *)malloc(sizeof(t_img));
	p = (t_img *)malloc(sizeof(t_img));
	while (w < WIN_W)
	{
		ft_ray(&env, &r, w);
		d->data = mlx_get_data_addr(env->img, &d->bps, &d->size, &d->end);
		p->data = mlx_get_data_addr(env->wall_pic, &p->bps, &p->size, &p->end);
		ft_draw(&env, w, d, p);
		d->data = mlx_get_data_addr(env->img, &d->bps, &d->size, &d->end);
		ft_sky(&env, w, d);
		d->data = mlx_get_data_addr(env->img, &d->bps, &d->size, &d->end);
		ft_ground(&env, w, d);
		w++;
	}
	return (env);
}

t_wolf	*ft_init(t_wolf *p, int h, int w)
{
	int		y;
	int		x;

	p->dirx = -1;
	p->diry = 0;
	p->planex = 0;
	p->planey = tan(60 * 2);
	y = h - 3;
	while (y < h)
	{
		x = w - 3;
		while (x < w)
		{
			if (p->world[y][x] == 0)
			{
				p->posx = x;
				p->posy = y;
				return (p);
			}
			x++;
		}
		y++;
	}
	return (p);
}
