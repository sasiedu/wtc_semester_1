/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo3.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/10 10:58:42 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:08:39 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_process_echo3(t_shell *ev, char *str, t_echo *inf)
{
	int		size;

	size = ft_strlen(str) - 1;
	if (ft_strlen(str) == 1 || str[size] == '*')
		ft_all_curr(ev, str, inf);
	else
		ft_all_curr(ev, str, inf);
}

int		ft_all_curr(t_shell *ev, char *str, t_echo *inf)
{
	DIR				*d;
	struct dirent	*dp;
	char			*path;
	t_list			*lst;
	int				size;

	path = ft_chr(ev->currdir, '=');
	d = opendir(path);
	if (d == NULL)
		return (write(1, "error : failed to open directory", 33) - 33);
	dp = readdir(d);
	ft_read_list(&lst, dp, d);
	lst = ft_arrange(&lst);
	size = ft_strlen(str) - 1;
	if (str[size] == '*')
		ft_print_end(lst, str, inf->fd);
	else if (ft_strlen(str) == 1)
		ft_print_curr(lst, inf->fd);
	else
		ft_print_frnt(lst, str, inf->fd);
	ft_lstdel(&lst, &ft_del);
	return (0);
}
