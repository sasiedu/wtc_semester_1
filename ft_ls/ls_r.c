/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_r.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 09:54:13 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 16:58:41 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

t_list		*ft_ls_r(t_list **ls, int size)
{
	t_list		*temp;
	int			i;

	temp = NULL;
	i = 0;
	while (i < size)
	{
		ft_rev(&temp, &(*ls));
		free((*ls)->content);
		*ls = (*ls)->next;
		i++;
	}
	return (temp);
}

void		ft_rev(t_list **temp, t_list **ls)
{
	t_list		*t;

	t = NULL;
	if (*temp == NULL)
	{
		*temp = (t_list *)malloc(sizeof(t_list));
		(*temp)->content = ft_memalloc(ft_strlen((*ls)->content) + 1);
		if ((*temp)->content == NULL)
			exit(-1);
		ft_strncpy((*temp)->content, (*ls)->content, ft_strlen((*ls)->content));
		(*temp)->next = NULL;
		return ;
	}
	t = (t_list *)malloc(sizeof(t_list));
	t->content = ft_memalloc(ft_strlen((*ls)->content) + 1);
	ft_strncpy(t->content, (*ls)->content, ft_strlen((*ls)->content));
	t->next = *temp;
	*temp = t;
	t = NULL;
}
