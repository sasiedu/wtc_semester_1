/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graph_tools2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 12:06:07 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:14:18 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_add_room(t_room **rooms, t_room *room)
{
	if (*rooms == NULL)
	{
		*rooms = room;
		(*rooms)->next = NULL;
		return ;
	}
	(*rooms)->prev = *rooms;
	ft_add_room(&(*rooms)->next, room);
}

t_room	*build_room(char *line, int start, int end, int num_ants)
{
	t_room	*room;
	char	**s;

	if ((room = (t_room *)malloc(sizeof(t_room))) == NULL)
		ft_error(1, "room");
	s = ft_strsplit(line, ' ');
	room->name = ft_strjoin(s[0], "");
	room->occupied = 0;
	room->x = ft_atoi(s[1]);
	room->y = ft_atoi(s[2]);
	room->start = start;
	room->end = end;
	room->edge = NULL;
	room->ant_id = 0;
	if (start == 1)
		room->num_ants = num_ants;
	else
		room->num_ants = 0;
	room->edge = NULL;
	room->next = NULL;
	room->prev = NULL;
	ft_free_2darray(s);
	return (room);
}

void	ft_build_ant(t_ant *ant, int i)
{
	ant->id = i + 1;
	ant->path = NULL;
	ant->end = 0;
}

t_path	*ft_find_ant_path(t_lemin **lem, t_ways **ways)
{
	if (*ways == NULL)
		return (NULL);
	if ((*ways)->path->next->room->occupied == 0)
	{
		(*ways)->path->next->room->occupied = 1;
		return ((*ways)->path);
	}
	return (ft_find_ant_path(&(*lem), &(*ways)->next));
}

void	ft_init_ants(t_lemin **lem, int size)
{
	int		i;

	(*lem)->num_ants = size;
	if (((*lem)->ants = (t_ant *)malloc(sizeof(t_ant) * size)) == NULL)
		exit(-1);
	i = 0;
	while (i < size)
	{
		ft_build_ant(&(*lem)->ants[i], i);
		i++;
	}
}
