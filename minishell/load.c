/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/06 19:25:16 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:54:52 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_load(t_shell **ev, char **env, t_list **hold)
{
	int		i;

	i = 0;
	while (env[i])
	{
		if (ft_strncmp("TMPDIR=", env[i], 7) == 0)
			(*ev)->temp = ft_strjoin(env[i], "");
		else if (ft_strncmp("USER=", env[i], 5) == 0)
			(*ev)->user = ft_strjoin(env[i], "");
		else if (ft_strncmp("PATH=", env[i], 5) == 0)
			(*ev)->bin = ft_strjoin(env[i], "");
		else if (ft_strncmp("PWD=", env[i], 4) == 0)
			(*ev)->currdir = ft_strjoin(env[i], "");
		else if (ft_strncmp("HOME=", env[i], 5) == 0)
			(*ev)->home = ft_strjoin(env[i], "");
		else if (ft_strncmp("OLDPWD=", env[i], 7) == 0)
			(*ev)->olddir = ft_strjoin(env[i], "");
		else if (ft_strncmp("_=", env[i], 2) == 0)
			(*ev)->end = ft_strjoin(env[i], "");
		if (i > 0)
			ft_lstaddend(&(*hold), ft_lstnew(env[i], 0));
		i++;
	}
	(*ev)->end[ft_strlen((*ev)->end) - 17] = '\0';
	(*ev)->oldline = NULL;
}

void	ft_exec(t_shell **ev, char **env)
{
	char	**split;

	split = ft_strsplit((*ev)->line, ' ');
	if (ft_strcmp("cd", split[0]) == 0)
		ft_do_cd(&(*ev), split);
	else if (ft_strcmp("pwd", split[0]) == 0)
		ft_pwd(*ev, split);
	else if (ft_strcmp("env", split[0]) == 0)
		ft_print_env(*ev);
	else if (ft_strcmp("exit", split[0]) == 0)
		exit(0);
	else if (ft_strcmp("setenv", split[0]) == 0)
		ft_setenv(&(*ev), split);
	else if (ft_strcmp("unsetenv", split[0]) == 0)
		ft_unsetenv(&(*ev), split);
	else if ((ft_strcmp("echo", split[0]) == 0 || \
			ft_strcmp("ECHO", split[0]) == 0) && ft_check_echo2(split) == 0)
		ft_do_echo(&(*ev), split);
	else
		ft_do_bin(&(*ev), split, env);
	ft_free_double(split);
}
