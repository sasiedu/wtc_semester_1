/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wall.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 14:32:37 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/24 12:23:01 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_wall(t_wolf **e, t_cast **r, t_util *u, int side)
{
	double		x;
	double		y;

	x = ((*r)->mapx - (*r)->rayposx + (1 - u->stepx) / 2) / (*r)->raydirx;
	y = ((*r)->mapy - (*r)->rayposy + (1 - u->stepy) / 2) / (*r)->raydiry;
	if (side == 0)
		(*r)->len = fabs(x);
	else
		(*r)->len = fabs(y);
	(*e)->wall = WIN_H / (*r)->len;
	(*e)->start = (WIN_H / 2) - ((*e)->wall / 2);
	if ((*e)->start < 0)
	{
		(*e)->tex_x = -1 * (*e)->start;
		(*e)->start = 0;
	}
	(*e)->side = side;
}

void	ft_color(t_wolf **e, t_cast **r)
{
	int		h;
	int		w;

	h = (*r)->mapx;
	w = (*r)->mapy;
	if ((*e)->world[h][w] == 1)
		(*e)->color = 0210045120;
	else if ((*e)->world[h][w] == 2)
		(*e)->color = 0120200050;
	else if ((*e)->world[h][w] == 3)
		(*e)->color = 0100220120;
	else if ((*e)->world[h][w] == 4)
		(*e)->color = 0004120145;
	else
		(*e)->color = 0120120100;
	if ((*e)->side == 1)
		(*e)->color = (*e)->color / 2;
}

void	ft_draw(t_wolf **e, int w, t_img *d, t_img *p)
{
	int		w1;
	int		h1;
	int		h;
	int		y;
	int		i;
	int		a;

	h = 0;
	y = (*e)->start;
	if ((*e)->start == 0 && (w != 0 || w != WIN_W - 1))
		w1 = (w % (*e)->pw) + (*e)->tex_x;
	else
		w1 = w % (*e)->pw;
	while (h <= (*e)->wall && y < WIN_H)
	{
		h1 = ((h * (*e)->ph) / (*e)->wall);
		//h1 = (*e)->tex_x;
		i = (w * 4) + (y * d->size);
		a = (w1 * 4) + (h1 * p->size);
		ft_memcpy(&d->data[i], &p->data[a], 4);
		h++;
		y++;
	}
}
