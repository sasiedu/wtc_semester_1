/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util_echo.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/10 12:30:26 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:54:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_print_end(t_list *ls, char *str, int fd)
{
	t_list	*t;
	int		size;
	char	*src;
	int		count;

	size = ft_strlen(str) - 1;
	t = ls;
	count = 0;
	while (t != NULL)
	{
		src = (char *)t->content;
		if (src[0] != '.' && ft_strncmp(src, str, size) == 0)
		{
			if (count > 0)
				write(fd, " ", 1);
			write(fd, src, ft_strlen(src));
			count++;
		}
		t = t->next;
	}
}

void	ft_print_frnt(t_list *ls, char *str, int fd)
{
	t_list	*t;
	size_t	size;
	size_t	size2;
	char	*src;
	int		count;

	size = ft_strlen(str) - 1;
	t = ls;
	count = 0;
	while (t != NULL)
	{
		src = (char *)t->content;
		size2 = ft_strlen(src) - size;
		if (src[0] != '.' && ft_strlen(src) >= size && \
				ft_strncmp(&str[1], &src[size2], size) == 0)
		{
			if (count > 0)
				write(fd, " ", 1);
			write(fd, src, ft_strlen(src));
			count++;
		}
		t = t->next;
	}
}

int		ft_check_echo2(char **split)
{
	int		i;
	int		j;

	j = 0;
	while (split[j] != NULL)
	{
		i = 0;
		while (split[j][i])
		{
			if (split[j][i] == '$' || split[j][i] == '#')
				return (1);
			i++;
		}
		j++;
	}
	return (0);
}
