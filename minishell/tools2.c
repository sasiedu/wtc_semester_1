/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 14:56:41 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 10:16:34 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_init_echo(t_echo **inf)
{
	(*inf)->e = 0;
	(*inf)->ee = 0;
	(*inf)->n = 0;
	(*inf)->wr = 0;
	(*inf)->app = 0;
}

int		ft_size_double(char **str)
{
	int		i;

	i = 0;
	while (str[i] != NULL)
		i++;
	return (i);
}

void	ft_check_echo(t_echo **inf, char **split)
{
	int		i;

	i = 0;
	while (split[i] != NULL)
	{
		if (split[i][0] == '-')
			ft_get_flags(&(*inf), split[i]);
		else
			ft_get_flags2(&(*inf), split[i]);
		i++;
	}
}

void	ft_get_flags(t_echo **inf, char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] == 'e')
		{
			(*inf)->e = 1;
			(*inf)->ee = 0;
		}
		else if (str[i] == 'E')
		{
			(*inf)->ee = 1;
			(*inf)->e = 0;
		}
		else if (str[i] == 'n')
			(*inf)->n = 1;
		i++;
	}
}

void	ft_get_flags2(t_echo **inf, char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (ft_strequ(">>", str) == 1)
			(*inf)->app = 1;
		else if (ft_strequ(">", str) == 1)
			(*inf)->wr = 1;
		i++;
	}
}
