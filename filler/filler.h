/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 22:22:48 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 13:43:51 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef	FILLER_H
# define FILLER_H

#include "libft.h"
#include "ft_printf.h"
#include "get_next_line.h"

typedef	struct		s_filler
{
	int				pl_n;
	int				b_h;
	int				b_w;
	int				pi_h;
	int				pi_w;
	int				pi_sw;
	int				pi_sh;
	int				start;
	char			pl[2];
	char			op[2];
	char			**b;
	char			**pi;
	char			*line;
}					t_filler;

void				ft_player_info(t_filler **mac);
void				ft_player_one(t_filler **mac);
void				ft_player_two(t_filler **mac);
void				ft_build_grid(t_filler **mac);
void				ft_fill(t_filler **mac);
void				ft_check(char *str, t_filler **mac, int h);
void				ft_build_piece(t_filler **mac);
void				ft_fill_p(t_filler **mac);
void				ft_start(t_filler **mac, int h);
void				ft_start2(t_filler **mac);
void				ft_play(t_filler **mac);
int					loop_up(t_filler **m);
int					ft_size1(t_filler **mac, int w, int h);
int					ft_try1(t_filler **mac, int w, int h, int count);
int					move_down(t_filler **m, int h);
void				ft_play2(t_filler **mac);
int					move_up(t_filler **m);

#endif
