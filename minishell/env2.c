/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/08 09:46:24 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/08 10:33:24 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_unsetenv(t_shell **ev, char **split)
{
	int		ret;
	int		i;

	i = 0;
	while (split[i])
		i++;
	if (i != 2)
	{
		ft_printf("error : setenv: error: too many arguments to function");
		ft_printf(" call, expected 1, have %d\n", ft_abs(i - 1));
		return ;
	}
	ret = ft_update(&(*ev)->hold, split);
	if (ret == 0)
	{
		ft_printf("error : %s: environment not found\n", split[1]);
		return ;
	}
	ft_setenv2(&(*ev), split, &(*ev)->hold, 0);
}
