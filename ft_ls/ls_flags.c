/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_flags.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 08:33:12 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 17:37:51 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int			ft_ls_flags(char **argv, char *path)
{
	DIR					*d;
	struct dirent		*dp;
	t_list				*ls;

	if ((d = opendir(path)) == NULL)
		return (0);
	dp = readdir(d);
	ft_read(&ls, dp, d);
	ls = ft_arrange(&ls);
	closedir(d);
	if (ft_strchr(argv[1], 'R') != NULL)
		ft_printf("%s/:\n", path);
	if (ft_strchr(argv[1], 'a') == NULL)
		ft_ls_a(&ls);
	ft_search_flags(argv[1], &ls, path);
	if (ft_strchr(argv[1], 'l') != NULL)
		ft_print_l(ls, path);
	if (ft_strchr(argv[1], 'R') != NULL)
		ft_search_dir(argv, path, &ls);
	ft_memdel((void **)&ls);
	return (1);
}

void		ft_ls_flags2(int argc, char **argv)
{
	int				i;

	i = argc - 1;
	while (i > 1)
	{
		if (argc > 3)
			ft_printf("%s:\n", argv[i]);
		if (ft_ls_flags(argv, argv[i]) == 0)
			ft_printf("%s: %s: No such file or directory\n", argv[0], argv[i]);
		i--;
	}
}

void		ft_search_flags(char *str, t_list **ls, char *path)
{
	int		size;

	size = ft_list_size(*ls);
	if (ft_strchr(str, 't') != NULL)
		*ls = ft_ls_t(&(*ls), path);
	if (ft_strchr(str, 'r') != NULL)
		*ls = ft_ls_r(&(*ls), size);
	if (ft_strchr(str, 'l') == NULL)
		ft_print_ls(*ls);
}
