/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/21 09:57:55 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:30:05 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		ft_check_ants(char *line)
{
	char	**s;

	s = ft_strsplit(line, ' ');
	if (ft_strsplit_size(s) != 1)
		return (0);
	return (1);
}

void	ft_check_rooms(t_lemin **lem)
{
	if ((*lem)->rooms == NULL)
		ft_error(3, "rooms");
	if ((*lem)->start == NULL)
		ft_error(3, "start");
	if ((*lem)->end == NULL)
		ft_error(3, "end");
}

void	ft_check_ways(t_lemin **lem)
{
	if ((*lem)->ways == NULL)
		ft_error(4, "NULL");
}
