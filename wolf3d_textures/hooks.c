/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 17:10:02 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/24 11:59:39 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"
#include <stdio.h>
int		key_hook(int keycode, t_wolf *e)
{
	mlx_destroy_image(e->mlx, e->img);
	e->img = mlx_new_image(e->mlx, WIN_W, WIN_H);
	if (keycode == 126)
		e = key_up(e);
	if (keycode == 125)
		e = key_down(e);
	if (keycode == 123)
		e = key_right(e);
	if (keycode == 124)
		e = key_left(e);
	if (keycode == 1)
		ft_left(&e, -1);
	if (keycode == 3)
		ft_left(&e, 1);
	if (keycode == 53)
		exit(0);
	if (keycode)
		ft_printf("%d\n", keycode);
	printf("%f %f\n", e->dirx, e->diry);
	e = ft_cast(e);
	mlx_clear_window(e->mlx, e->win);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	return (0);
}

void	ft_left(t_wolf **e, int i)
{
	double		old;
	double		oldp;
	double		tt;

	old = (*e)->diry;
	oldp = (*e)->planey;
	tt = 0.073;
	(*e)->diry = (*e)->diry * cos(i * tt) - (*e)->dirx * sin(i * tt);
	(*e)->dirx = old * sin(i * tt) + (*e)->dirx * cos(i * tt);
	(*e)->planey = (*e)->planey * cos(i * tt) - (*e)->planex * sin(i * tt);
	(*e)->planex = oldp * sin(i * tt) + (*e)->planex * cos(i * tt);
}

int		get_x(t_wolf **e, t_cast **r)
{
	double		wallx;
	int			x;

	if ((*e)->side == 0)
		wallx = (*r)->rayposy + (*r)->len * (*r)->raydiry;
	else
		wallx = (*r)->rayposx + (*r)->len * (*r)->raydirx;

	x = (int)wallx  * (double)(*e)->pw;
	if ((*e)->side == 0 && (*r)->raydirx > 0)
		x = (*e)->pw - x - 1;
	else if ((*e)->side == 1 && (*r)->raydiry < 0)
		x = (*e)->pw - x - 1;
	return (x);
}
