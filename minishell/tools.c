/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/06 20:49:06 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/09 14:56:34 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

char	*ft_chr(char *str, char c)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (&str[i + 1]);
		i++;
	}
	return (NULL);
}

char	*ft_get_path(char *path, t_shell *env, char *temp)
{
	if (path == NULL)
		return (NULL);
	if (path[0] == '.' && path[1] && path[1] == '/')
		temp = ft_strjoin(ft_chr(env->currdir, '='), &path[1]);
	else if (path[0] == '.' && ft_strlen(path) == 1)
		temp = ft_strjoin(ft_chr(env->currdir, '='), "");
	else if (path[0] == '~' && path[1] && path[1] == '/')
		temp = ft_strjoin(ft_chr(env->home, '='), &path[1]);
	else if (path[0] == '~' && ft_strlen(path) == 1)
		temp = ft_strjoin(ft_chr(env->home, '='), "");
	else if (ft_strlen(path) > 1 && path[0] == '/' && path[1] == '/')
		temp = ft_strjoin("/", "");
	else if (path[0] == '-' && ft_strlen(path) == 1)
		temp = ft_strjoin(ft_chr(env->olddir, '='), "");
	else if (ft_strlen(path) == 2 && path[0] == '-' && path[1] == '-')
		temp = ft_strjoin(ft_chr(env->home, '='), "");
	else if (path[0] != '.' && path[0] != '/' && \
		env->currdir[ft_strlen(env->currdir) - 1] != '/')
		temp = ft_strjoin(ft_chr(env->currdir, '='), ft_strjoin("/", path));
	else if (path != NULL && path[0] != '.' && path[0] != '/')
		temp = ft_strjoin(ft_chr(env->currdir, '='), path);
	else
		temp = ft_strjoin(path, "");
	return (temp);
}

int		ft_check_dir(char *path, t_shell *env, char **temp)
{
	char			*c;
	char			*acc;
	struct stat		*buf;
	int				ret;
	char			*str;

	str = NULL;
	*temp = ft_get_path(path, env, str);
	ret = 0;
	buf = (struct stat *)malloc(sizeof(struct stat));
	c = (char *)malloc(sizeof(char) * 3);
	acc = NULL;
	if (lstat(*temp, buf) == -1)
		ret = -1;
	else if (ret == 0)
		acc = ft_uni_convert(buf->st_mode, 8);
	if (ret == 0 && ft_atoi(&acc[ft_strlen(acc) - 3]) < 400)
		ret = -2;
	if (ret == 0 && ft_atoi(ft_strncpy(c, acc, ft_strlen(acc) - 4)) != 4)
		ret = -3;
	ft_memdel((void **)&acc);
	ft_memdel((void **)&c);
	ft_memdel((void **)&buf);
	return (ret);
}
