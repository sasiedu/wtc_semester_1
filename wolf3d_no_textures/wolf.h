/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 16:29:38 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/23 17:03:42 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include "ft_printf.h"
# include "get_next_line.h"

# define WIN_H 600
# define WIN_W 1000
# define PI 3.14159
# define FOV 60
# define KEYPRESSMASK (1L<<0)

typedef	struct		s_wolf
{
	void			*mlx;
	void			*win;
	void			*img;
	int				**world;
	int				h;
	int				w;
	int				color;
	char			*line;
	double			posx;
	double			posy;
	double			dirx;
	double			diry;
	double			planex;
	double			planey;
	double			wall;
	int				start;
	int				side;
}					t_wolf;

typedef	struct		s_cast
{
	double			rayposx;
	double			rayposy;
	double			raydirx;
	double			raydiry;
	double			deltax;
	double			deltay;
	double			len;
	int				mapx;
	int				mapy;
}					t_cast;

typedef	struct		s_util
{
	double			sidex;
	double			sidey;
	int				stepx;
	int				stepy;
}					t_util;

typedef	struct		s_img
{
	char			*data;
	int				bps;
	int				size;
	int				end;
}					t_img;

int					ft_wolf(char **argv);
int					ft_check_world(char **argv, t_wolf **env);
int					ft_build_map(char **argv, t_wolf **env, int w, int h);
int					expose_hook(t_wolf *env);
t_wolf				*ft_cast(t_wolf *env);
t_wolf				*ft_init(t_wolf *p, int h, int w);
void				ft_ray(t_wolf **e, t_cast **r, int w);
void				ft_util(t_wolf **e, t_cast **r);
void				ft_raylen(t_wolf **e, t_cast **r, t_util *u);
void				ft_wall(t_wolf **e, t_cast **r, t_util *u, int side);
void				ft_color(t_wolf **e, t_cast **r);
void				ft_draw(t_wolf **e, int w, t_img *d);
void				ft_sky(t_wolf **e, int w, t_img *d);
void				ft_ground(t_wolf **e, int w, t_img *d);
int					key_hook(int keycode, t_wolf *e);
void				ft_left(t_wolf **e, int i);
t_wolf				*key_up(t_wolf *e);
t_wolf				*key_down(t_wolf *e);
t_wolf				*key_left(t_wolf *e);
t_wolf				*key_right(t_wolf *e);

#endif
