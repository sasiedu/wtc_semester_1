
#include "libft.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}


char	pop1()
{
	char		c;
	t_stack1	*temp;

	c = g_ops->c;
	temp = g_ops->next;
	g_ops = temp;
	return (c);
}

void	eval()
{
	int		a;
	int		b;
	char	c;

	c = pop1();
	if (c == '(')
		return ;
	a = pop();
	b = pop();
	if (c == '+')
		push(b + a);
	else if (c == '-')
		push(b - a);
	else if (c == '*')
		push(b * a);
	else if (c == '/')
		push(b / a);
	else if (c == '%')
		push(b % a);
}

int		ft_size()
{
	int			i;
	t_stack		*temp;

	temp = g_digit;
	i = 0;
	while (temp)
	{
		temp = temp->next;
		i++;
	}
	return (i);
}
