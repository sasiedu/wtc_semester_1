/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/21 10:07:23 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:32:46 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_error(int err, char *str)
{
	if (err == 1)
		ft_printf("error : failed to allocate memory for %s. program exited\n", str);
	else if (err == 2)
		ft_printf("error : no ants to move. Program exited\n");
	else if (err == 3)
		ft_printf("error : no %s in map. Program exited\n", str);
	else if (err == 4)
		ft_printf("error : no possible path available. Program exited\n");
	exit(1);
}
