/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 16:50:32 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/21 11:26:04 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int			ft_check_world(char **argv, t_wolf **env)
{
	int		fd;
	int		h;

	h = 0;
	if ((fd = open(argv[1], O_RDONLY)) == -1)
		return (-1);
	while (get_next_line(fd, &(*env)->line) == 1)
	{
		if (h == 0)
			(*env)->w = ft_strlen((*env)->line);
		if (h > 0 && (*env)->w != (int)ft_strlen((*env)->line))
			return (-1);
		h++;
	}
	(*env)->h = h;
	close(fd);
	ft_printf("h : %i    w : %i\n", (*env)->h, (*env)->w);
	return (0);
}

int			ft_build_map(char **argv, t_wolf **env, int h, int w)
{
	int		i;
	int		x;
	int		fd;
	char	*line;

	i = 0;
	if ((fd = open(argv[1], O_RDONLY)) == -1)
		return (-1);
	(*env)->world = (int **)malloc(sizeof(int *) * h);
	while (i < h)
	{
		get_next_line(fd, &line);
		(*env)->world[i] = (int *)malloc(sizeof(int) * w);
		x = 0;
		while (line[x])
		{
			(*env)->world[i][x] = ft_atoi(ft_strsub(line, x, 1));
			x++;
		}
		i++;
	}
	return (0);
}
