/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 21:19:15 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:15:23 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_moving_ants(t_lemin **lem)
{
	while ((*lem)->end->num_ants != (*lem)->num_ants)
	{
		ft_move_ant2(&(*lem));
	}
}

void	ft_move_ant2(t_lemin **lem)
{
	int		i;

	i = 0;
	while (i < (*lem)->num_ants)
	{
		if ((*lem)->ants[i].path == NULL)
			(*lem)->ants[i].path = ft_find_ant_path(&(*lem), &(*lem)->ways);
		i++;
	}
	i = 0;
	while (i < (*lem)->num_ants)
	{
		if ((*lem)->ants[i].path != NULL)
		{
			ft_move_ant(&(*lem)->rooms, &(*lem)->ants[i]);
			if ((*lem)->ants[i].end != 1)
				ft_printf("L%d-%s ", (*lem)->ants[i].id, \
						(*lem)->ants[i].path->room->name);
			if ((*lem)->ants[i].path->room->end == 1)
				(*lem)->ants[i].end = 1;
		}
		i++;
	}
	ft_reset_way(&(*lem)->ways);
	ft_printf("\n");
}

void	ft_reset_way(t_ways **ways)
{
	if (*ways == NULL)
		return ;
	(*ways)->path->next->room->occupied = 0;
	return (ft_reset_way(&(*ways)->next));
}
