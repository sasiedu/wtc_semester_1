/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/06 20:33:34 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/07 14:08:09 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_do_cd(t_shell **ev, char **split)
{
	int		size;
	char	*temp;

	size = 0;
	while (split[size])
		size++;
	temp = (*ev)->olddir;
	if (size == 1)
	{
		ft_memdel((void **)&(*ev)->olddir);
		(*ev)->olddir = ft_strjoin("OLD", (*ev)->currdir);
		(*ev)->currdir = ft_strjoin("PWD=", ft_chr((*ev)->home, '='));
		chdir(ft_chr((*ev)->home, '='));
	}
	else if (size > 1)
		ft_do_cd2(&(*ev), split);
}

void	ft_do_cd2(t_shell **ev, char **split)
{
	int		ret;
	char	*temp;
	char	*buf;

	buf = NULL;
	ret = ft_check_dir(split[1], *ev, &temp);
	if (ret == -1)
		ft_printf("-bash: cd: %s: No such file or directory\n", split[1]);
	else if (ret == -2)
		ft_printf("-bash: cd: %s: Permission denied\n", split[1]);
	else if (ret == -3)
		ft_printf("-bash: cd: %s: Not a directory\n", split[1]);
	else if (ret == 0)
	{
		free((*ev)->olddir);
		(*ev)->olddir = ft_strjoin("OLD", (*ev)->currdir);
		if (temp[ft_strlen(temp) - 1] != '/')
			temp = ft_strjoin(temp, "/");
		free((*ev)->currdir);
		(*ev)->currdir = ft_strjoin("PWD=", temp);
		chdir(temp);
	}
	ft_memdel((void **)&temp);
}
