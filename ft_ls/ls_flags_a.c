/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_flags_a.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 09:22:50 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 17:51:04 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int			ls_search(char *str)
{
	if (str[0] == '.' || str[0] == '+')
		return (0);
	return (1);
}

void		ft_ls_a(t_list **ls)
{
	while (*ls != NULL)
	{
		if (ls_search((char *)(*ls)->content) == 1)
			return ;
		free((*ls)->content);
		*ls = (*ls)->next;
	}
}

int			ls_rec_search(char *str)
{
	if (ft_strlen(str) == 1 && str[0] == '.')
		return (0);
	if (ft_strlen(str) == 2 && str[1] == '.')
		return (0);
	return (1);
}
