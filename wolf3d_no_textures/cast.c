/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cast.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 12:22:55 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/23 17:22:21 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_ray(t_wolf **e, t_cast **r, int w)
{
	double		camx;
	double		a;
	double		x;
	double		y;

	camx = (2.0 * w / (double)WIN_W) - 1;
	(*r)->rayposx = (*e)->posx;
	(*r)->rayposy = (*e)->posy;
	(*r)->raydirx = (*e)->dirx + (*e)->planex * camx;
	(*r)->raydiry = (*e)->diry + (*e)->planey * camx;
	(*r)->mapx = (int)(*r)->rayposx;
	(*r)->mapy = (int)(*r)->rayposy;
	x = (*r)->raydirx;
	y = (*r)->raydiry;
	a = 1 + (y * y) / (x * x);
	(*r)->deltax = sqrt(a);
	a = 1 + (x * x) / (y * y);
	(*r)->deltay = sqrt(a);
	ft_util(&(*e), &(*r));
	ft_color(&(*e), &(*r));
}

void	ft_util(t_wolf **e, t_cast **r)
{
	t_util		*u;

	u = (t_util *)malloc(sizeof(t_util));
	if ((*r)->raydirx - 0.0 < 0.00000)
	{
		u->stepx = -1;
		u->sidex = ((*r)->rayposx - (*r)->mapx) * (*r)->deltax;
	}
	else
	{
		u->stepx = 1;
		u->sidex = ((*r)->mapx + 1.0 - (*r)->rayposx) * (*r)->deltax;
	}
	if ((*r)->raydiry - 0.0 < 0.00000)
	{
		u->stepy = -1;
		u->sidey = ((*r)->rayposy - (*r)->mapy) * (*r)->deltay;
	}
	else
	{
		u->stepy = 1;
		u->sidey = ((*r)->mapy + 1.0 - (*r)->rayposy) * (*r)->deltay;
	}
	ft_raylen(&(*e), &(*r), u);
	ft_memdel((void **)&u);
}

void	ft_raylen(t_wolf **e, t_cast **r, t_util *u)
{
	int		side;
	int		hit;

	hit = 0;
	while (hit == 0)
	{
		if (u->sidex - u->sidey < 0.00000)
		{
			u->sidex += (*r)->deltax;
			(*r)->mapx += u->stepx;
			side = 0;
		}
		else
		{
			u->sidey += (*r)->deltay;
			(*r)->mapy += u->stepy;
			side = 1;
		}
		if ((*e)->world[(*r)->mapy][(*r)->mapx] > 0)
			hit = 1;
	}
	ft_wall(&(*e), &(*r), u, side);
}
