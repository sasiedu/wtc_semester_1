/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 10:20:55 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:32:51 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include "libft.h"
# include "ft_printf.h"
# include "get_next_line.h"

typedef	struct		s_route
{
	char			*room;
	struct s_route	*next;
}					t_route;

typedef	struct		s_room
{
	char			*name;
	int				occupied;
	int				x;
	int				y;
	int				start;
	int				end;
	int				num_ants;
	int				ant_id;
	t_route			*edge;
	struct s_room	*next;
	struct s_room	*prev;
}					t_room;

typedef	struct		s_path
{
	t_room			*room;
	struct s_path	*next;
}					t_path;

typedef	struct		s_ways
{
	t_path			*path;
	struct s_ways	*next;
}					t_ways;

typedef	struct		s_ant
{
	int				id;
	int				end;
	t_path			*path;
}					t_ant;

typedef	struct		s_lemin
{
	int				num_ants;
	int				num_rooms;
	int				num_paths;
	char			**links;
	t_room			*rooms;
	t_room			*start;
	t_room			*end;
	t_ant			*ants;
	t_ways			*ways;
}					t_lemin;
/*
*** utilities functions
*/
char				**ft_add_2darray(char **save, char *add);
size_t				ft_strsplit_size(char **str);

/*
*** free functions
*/
void				ft_free_2darray(char **str);
void				ft_list_remove_if(t_list **lst, void *data, int (*cmp)());

/*
*** lemin functions
*/
void				ft_find_path(t_lemin **lem, t_room **room, \
		t_list **save, char *str);
void				ft_check_path(t_list **lst);
void				ft_compare_path(t_list **lst, char *path);
int					ft_compare_name(const char *dest, \
		const char *src);
void				ft_build_ways(t_lemin **lem, t_ways **ways, t_list **lst);
void				ft_add_ways(t_lemin **lem, t_ways **ways, char *str);
void				ft_add_rooms_path(t_lemin **lem, t_path **path, char *str);
void				ft_moving_ants(t_lemin **lem);
void				ft_move_ant2(t_lemin **lem);

/*
*** error functions
*/
void				ft_error(int err, char *str);

/*
*** check functions
*/
int					ft_check_ants(char *line);
void				ft_check_rooms(t_lemin **lem);
void				ft_check_ways(t_lemin **lem);

/*
*** printing functions
*/
void				ft_print_rooms(t_room **rooms);
void				ft_print_2darray(char **str);
void				ft_print_roomlinks(t_room **room);
void				ft_print_list(t_list *lst);
void				ft_print_ways(t_ways **ways);

/*
*** graph tools functions
*/
void				ft_add_roomlink(t_route **edge, char *name);
void				ft_move_ant(t_room **room, t_ant *ant);
void				ft_set_room_busy(t_room **room, char *name);
void				ft_set_room_empty(t_room **room, char *name);
void				ft_add_room(t_room **rooms, t_room *room);
void				ft_build_ant(t_ant *ant, int i);
void				ft_reset_way(t_ways **ways);
t_room				*ft_find_room(t_room **room, char *name);
t_room				*build_room(char *line, int start, int end, int num_ants);
t_path				*ft_find_ant_path(t_lemin **lem, t_ways **ways);

/*
*** initialisation functions
*/
void				ft_init_ants(t_lemin **lem, int size);
void				ft_init_rooms(t_lemin **lem);
void				ft_init_start_end(t_lemin **lem, char *prev);
void				ft_init_links(t_lemin **lem, char *line);
t_lemin				*ft_init_map(void);

#endif
