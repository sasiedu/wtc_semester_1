#include "libft.h"

void	eval_b()
{
	t_stack1	*temp;
	while (g_ops->c != '(')
		eval();
	temp = g_ops->next;
	g_ops = temp;
}

void	eval_ops(char c)
{
	if (c == ')')
		eval_b();
	else if (c == '-')
		eval_sub();
	else if (c == '+')
		eval_add();
	else if (c == '*')
		eval_mul();
	else if (c == '%')
		eval_mod();
	else if (c == '/')
		eval_div();
	push1(c);
}

int		ft_find(char *str, char c)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (1);
		i++;
	}
	return (0);
}

char	*get_num(char *base, char *expr)
{
	int		i;
	char	*temp;

	temp = (char*)malloc(sizeof(char) * 60);
	i = 0;
	while (ft_find(base, expr[g_i]) == 1)
	{
		temp[i] = expr[g_i];
		i++;
		g_i++;
	}
	g_i--;
	return (temp);
}

int		eval_expr(char *base, char *ops, char *expr)
{
	push1('(');
	while (expr[g_i])
	{
		if (ft_find(base, expr[g_i]) == 1)
			push(ft_uatoi(get_num(base, expr)));
		else if (ft_find(ops, expr[g_i]) == 1)
			eval_ops(expr[g_i]);
		g_i++;
	}
	while (g_ops)
		eval();
	printf("%d""\n", g_digit->x);
	free(g_digit);
	free(g_ops);
	return (0);
}
