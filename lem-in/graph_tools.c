/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   graph_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 11:08:07 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:13:30 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_add_roomlink(t_route **edge, char *name)
{
	if (*edge != NULL)
		return (ft_add_roomlink(&(*edge)->next, name));
	if ((*edge = (t_route *)malloc(sizeof(t_route))) == NULL)
		ft_error(1, "edge");
	(*edge)->room = ft_strjoin(name, "");
	(*edge)->next = NULL;
}

void	ft_move_ant(t_room **room, t_ant *ant)
{
	if (ant->path->room->end == 1)
		return ;
	ant->path->next->room->num_ants++;
	ant->path->next->room->ant_id = ant->id;
	ft_set_room_busy(&(*room), ant->path->next->room->name);
	ant->path->room->num_ants--;
	ant->path->room->ant_id = 0;
	ft_set_room_empty(&(*room), ant->path->room->name);
	ant->path = ant->path->next;
}

t_room	*ft_find_room(t_room **room, char *name)
{
	t_room	*temp;

	temp = *room;
	while (temp != NULL)
	{
		if (ft_strcmp(temp->name, name) == 0)
			return (temp);
		temp = temp->next;
	}
	return (NULL);
}

void	ft_set_room_busy(t_room **room, char *name)
{
	t_room	*temp;

	temp = *room;
	while (temp != NULL)
	{
		if (ft_strcmp(temp->name, name) == 0)
		{
			temp->occupied = 1;
			return ;
		}
		temp = temp->next;
	}
}

void	ft_set_room_empty(t_room **room, char *name)
{
	t_room	*temp;

	temp = *room;
	while (temp != NULL)
	{
		if (ft_strcmp(temp->name, name) == 0)
		{
			temp->occupied = 0;
			return ;
		}
		temp = temp->next;
	}
}
