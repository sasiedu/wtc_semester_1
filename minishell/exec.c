/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/07 01:07:27 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:54:45 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_do_bin(t_shell **ev, char **split, char **env)
{
	pid_t	proc;
	int		status;

	proc = fork();
	if (proc == 0)
		ft_bin(&(*ev), split, env);
	if (proc > 0)
		wait(&status);
}

void	ft_bin(t_shell **ev, char **split, char **env)
{
	char	**temp;
	char	*path;
	int		i;
	int		ret;

	i = 0;
	temp = ft_strsplit(ft_chr((*ev)->bin, '='), ':');
	while (temp[i] != NULL)
	{
		path = ft_strjoin(temp[i], ft_strjoin("/", split[0]));
		ret = execve(path, &split[0], &env[0]);
		if (ret != -1)
			break ;
		i++;
	}
	if (ft_strncmp("./", split[0], 2) == 0)
	{
		path = ft_strjoin(ft_chr((*ev)->currdir, '='), &split[0][1]);
		ret = execve(path, &split[0], &env[0]);
	}
	if (ret == -1)
		ft_printf("-bash: %s: command not found\n", split[0]);
}

void	ft_pwd(t_shell *ev, char **split)
{
	int		i;

	i = 0;
	while (split[i])
		i++;
	if (i > 1)
		ft_printf("pwd: too many arguments\n");
	else
		ft_printf("%s\n", ft_chr((ev)->currdir, '='));
}
