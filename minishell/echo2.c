/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 16:10:24 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:09:27 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_echo_flags_print(t_shell *ev, char *str, t_echo *inf, char **split)
{
	int		i;

	i = 1;
	while (str[i])
	{
		if (str[i] != 'e' && str[i] != 'E' && str[i] != 'n')
		{
			ft_print_echo(ev, str, inf, split);
			return ;
		}
		i++;
	}
}

int		ft_init_fd(t_echo *inf, char **split)
{
	int		size;
	int		fd;

	fd = 0;
	size = ft_size_double(split);
	if (inf->wr == 1)
	{
		fd = open(split[size - 1], O_WRONLY | O_CREAT, S_IRWXU | S_IRWXG);
	}
	else if (inf->app == 1)
		fd = open(split[size - 1], O_RDWR | O_CREAT | O_APPEND, \
		S_IRWXU | S_IRWXG);
	return (fd);
}

int		ft_process_e(int fd, char *str)
{
	int		i;

	i = 0;
	if (str[i + 1] && str[i + 1] == 'a')
		write(fd, "\a", 1);
	else if (str[i + 1] && str[i + 1] == 'b')
		write(fd, "\b", 1);
	else if (str[i + 1] && str[i + 1] == 'c')
		exit(1);
	else if (str[i + 1] && str[i + 1] == 'e')
		write(fd, "\e", 1);
	else if (str[i + 1] && str[i + 1] == 'f')
		write(fd, "\f", 1);
	else if (str[i + 1] && str[i + 1] == 'n')
		write(fd, "\n", 1);
	else if (str[i + 1] && str[i + 1] == 'r')
		write(fd, "\r", 1);
	else if (str[i + 1] && str[i + 1] == 't')
		write(fd, "\t", 1);
	else if (str[i + 1] && str[i + 1] == 'v')
		write(fd, "\v", 1);
	else if (str[i + 1] && str[i + 1] == '\\')
		write(fd, "\\", 1);
	return (1);
}
