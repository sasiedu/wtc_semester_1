/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: oexall <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/16 06:41:29 by oexall            #+#    #+#             */
/*   Updated: 2016/05/21 12:58:26 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include "libft.h"
#include "get_next_line.h"

void	open_read(char *filename, char *filename1, char *filename2, char *filename3);

int		main(int argc, char **argv)
{
	int	i;

	i = 1;
	if (i < argc)
	{
		printf("\n\nFILENAME: ++++++++++++++ %s ++++++++++++++\n\n", argv[i]);
		open_read(argv[1], argv[2], argv[3], argv[4]);
	}
}

void	open_read(char *filename, char *filename1, char *filename2, char *filename3)
{
	int		fd = 0;
	int		fd1 = 0;
	int		fd2 = 0;
	int		fd3 = 0;
	char	*line;

	fd = open(filename, O_RDONLY);
	fd1 = open(filename1, O_RDONLY);
	fd2 = open(filename2, O_RDONLY);
	fd3 = open(filename3, O_RDONLY);

	get_next_line(fd, &line);
	ft_putendl(line);
	get_next_line(fd1, &line);
	ft_putendl(line);
	get_next_line(fd2, &line);
	ft_putendl(line);
	get_next_line(fd3, &line);
	ft_putendl(line);
	get_next_line(fd, &line);
	ft_putendl(line);
	get_next_line(fd3, &line);
	ft_putendl(line);
	get_next_line(fd2, &line);
	ft_putendl(line);
}
