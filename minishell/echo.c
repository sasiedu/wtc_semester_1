/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/10 10:11:30 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/10 13:54:48 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.h"

void	ft_do_echo(t_shell **ev, char **split)
{
	t_echo		*inf;
	int			i;

	if ((inf = (t_echo *)malloc(sizeof(t_echo))) == NULL)
		return ;
	if (ft_size_double(split) == 1)
		return ;
	ft_init_echo(&inf);
	ft_check_echo(&inf, split);
	i = 1;
	while (split[i])
	{
		if (split[i][0] == '-')
			ft_echo_flags_print(*ev, split[i], inf, split);
		else if (ft_strcmp(">>", split[i]) == 0 || \
				ft_strcmp(">", split[i]) == 0)
			break ;
		else
			ft_print_echo(*ev, split[i], inf, split);
		i++;
	}
	if (inf->n != 1)
		write(inf->fd, "\n", 1);
}

void	ft_print_echo(t_shell *ev, char *str, t_echo *inf, char **split)
{
	int		s;

	s = ft_strlen(str) - 1;
	if (inf->wr == 1 || inf->app == 1)
	{
		if ((inf->fd = ft_init_fd(inf, split)) == -1)
		{
			ft_printf("error : permission denied or unable to create file\n");
			return ;
		}
	}
	else
		inf->fd = 1;
	if (ft_strchr(str, '*') != NULL && str[0] != 34 && str[0] != 39 && \
			str[s] != 39 && str[s] != 34)
		ft_process_echo3(ev, str, inf);
	else
		ft_print_echo2(ev, str, inf, inf->fd);
}

void	ft_print_echo2(t_shell *ev, char *str, t_echo *inf, int fd)
{
	int				i;

	i = 0;
	while (str[i])
	{
		if (inf->e == 1 && str[0] == 39 && str[ft_strlen(str) - 1] == 39 \
				&& str[i] == '\\')
			i = i + ft_process_e(fd, &str[i]);
		else if (inf->e == 1 && str[0] == 34 && str[ft_strlen(str) - 1] \
				== 34 && str[i] == '\\')
			i = i + ft_process_e(fd, &str[i]);
		else if (inf->ee == 1 && str[i] != 39 && str[i] != 34)
			write(fd, &str[i], 1);
		else if (str[i] != 39 && str[i] != 34 && ev->line != NULL)
			write(fd, &str[i], 1);
		i++;
	}
	write(1, " ", 1);
}
