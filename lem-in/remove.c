/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 15:46:41 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/20 16:04:27 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_list_remove_if(t_list **lst, void *data, int (*cmp)())
{
	t_list	*temp;

	if (*lst != NULL)
	{
		if (cmp((*lst)->content, data) == 0)
		{
			temp = *lst;
			*lst = (*lst)->next;
			ft_memdel(&temp->content);
			ft_memdel((void **)&temp);
		}
		else
			ft_list_remove_if(&(*lst)->next, data, cmp);
	}
}
