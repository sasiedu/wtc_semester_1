/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   info.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 23:34:20 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/18 11:21:29 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

int			ft_get_info(t_info **file, t_list **ls, char *str)
{
	struct stat				*buf;
	struct passwd			*uid;
	struct group			*gid;

	buf = (struct stat *)malloc(sizeof(struct stat));
	uid = (struct passwd *)malloc(sizeof(struct passwd));
	gid = (struct group *)malloc(sizeof(struct group));
	if (buf == NULL || uid == NULL || gid == NULL || *file == NULL)
		return (1);
	if (lstat(ft_get_path(str, (*ls)->content), buf) == -1)
		return (1);
	uid = getpwuid(buf->st_uid);
	gid = getgrgid(buf->st_gid);
	(*file)->name = ft_strjoin("", (*ls)->content);
	(*file)->pw_uid = uid->pw_name;
	(*file)->gw_gid = gid->gr_name;
	get_time((*ls)->content, &(*file)->clock, str);
	(*file)->file_size = buf->st_size;
	(*file)->n_links = buf->st_nlink;
	(*file)->chmod = (t_mode *)malloc(sizeof(t_mode));
	ft_get_chmod(&(*file)->chmod, ft_uni_convert(buf->st_mode, 8));
	buf = NULL;
	uid = NULL;
	gid = NULL;
	return (0);
}

void		ft_get_chmod(t_mode **chmod, char *str)
{
	int		i;
	char	*temp;

	temp = (char *)malloc(sizeof(char) * 3);
	i = ft_strlen(str) - 1;
	(*chmod)->other = ft_strjoin("", ft_mode(str[i]));
	(*chmod)->grp = ft_strjoin("", ft_mode(str[i - 1]));
	(*chmod)->usr = ft_strjoin("", ft_mode(str[i - 2]));
	(*chmod)->d = ft_dir(ft_atoi(ft_strncpy(temp, str, ft_strlen(str) - 4)));
}

char		ft_dir(int a)
{
	if (a == 1)
		return ('p');
	if (a == 2)
		return ('c');
	if (a == 4)
		return ('d');
	if (a == 6)
		return ('b');
	if (a == 10)
		return ('-');
	if (a == 12)
		return ('l');
	if (a == 14)
		return ('s');
	return ('w');
}

char		*ft_mode(char c)
{
	if (c == '7')
		return ("rwx");
	if (c == '6')
		return ("rw-");
	if (c == '5')
		return ("r-x");
	if (c == '4')
		return ("r--");
	if (c == '3')
		return ("-wx");
	if (c == '2')
		return ("-w-");
	if (c == '1')
		return ("--x");
	return ("---");
}
