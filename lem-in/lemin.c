/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 07:38:43 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/21 10:15:10 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	ft_find_path(t_lemin **lem, t_room **room, t_list **save, char *str)
{
	t_route		*temp;
	t_room		*temp_room;
	int			size;

	temp = (*room)->edge;
	while (temp != NULL)
	{
		temp_room = ft_find_room(&(*lem)->rooms, temp->room);
		size = ft_strlen(str);
		if (temp_room->end == 1)
		{
			str = ft_strjoin(str, ft_strjoin(" ", temp->room));
			ft_lstadd(&(*save), ft_lstnew(str, 0));
			return ;
		}
		if (ft_strstr(str, temp->room) == NULL)
		{
			str = ft_strjoin(str, ft_strjoin(" ", temp->room));
			ft_find_path(&(*lem), &temp_room, &(*save), str);
			str[size] = '\0';
		}
		temp = temp->next;
	}
}
