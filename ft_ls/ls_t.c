/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_t.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/11 16:42:23 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/17 17:07:31 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

t_list			*ft_ls_t(t_list **ls, char *path)
{
	t_list		*temp;
	int			i;
	int			size;

	temp = NULL;
	i = 0;
	size = ft_list_size(*ls);
	while (i < size)
	{
		ft_arrange_t(&temp, &(*ls), path);
		free((*ls)->content);
		*ls = (*ls)->next;
		i++;
	}
	return (temp);
}

void			ft_arrange_t(t_list **temp, t_list **ls, char *path)
{
	t_list		*t;

	t = NULL;
	if (*temp == NULL)
	{
		*temp = (t_list *)malloc(sizeof(t_list));
		(*temp)->content = ft_memalloc(ft_strlen((*ls)->content) + 1);
		if ((*temp)->content == NULL)
			exit(-1);
		ft_strncpy((*temp)->content, (*ls)->content, ft_strlen((*ls)->content));
		(*temp)->next = NULL;
		return ;
	}
	if (ft_time_compare((*temp)->content, (*ls)->content, path) == 1)
	{
		t = (t_list *)malloc(sizeof(t_list));
		t->content = ft_memalloc(ft_strlen((*ls)->content) + 1);
		ft_strncpy(t->content, (*ls)->content, ft_strlen((*ls)->content));
		t->next = *temp;
		*temp = t;
		t = NULL;
	}
	else
		ft_arrange_t(&(*temp)->next, &(*ls), path);
}

int				ft_time_compare(char *temp, char *str, char *path)
{
	long	int		t1;
	long	int		t2;
	struct stat		*buf;

	if ((buf = (struct stat *)malloc(sizeof(struct stat))) == NULL)
		exit(-1);
	if ((lstat(ft_get_path(path, temp), buf)) == -1)
		exit(-1);
	t1 = buf->st_mtimespec.tv_sec;
	if ((lstat(ft_get_path(path, str), buf)) == -1)
		exit(-1);
	t2 = buf->st_mtimespec.tv_sec;
	ft_memdel((void **)&buf);
	if (t1 < t2)
		return (1);
	return (0);
}
