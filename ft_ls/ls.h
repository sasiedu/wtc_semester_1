/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/09 11:43:41 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/18 11:19:31 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LS_H
# define LS_H

# include <dirent.h>
# include "libft.h"
# include "ft_printf.h"
# include <time.h>
# include <grp.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <pwd.h>
# include <uuid/uuid.h>

typedef	struct		s_time
{
	int				m_num;
	int				hh;
	int				mm;
	int				ss;
	int				yy;
	int				date;
	char			*mon;
}					t_time;

typedef	struct		s_mode
{
	char			d;
	char			*usr;
	char			*grp;
	char			*other;
}					t_mode;

typedef	struct		s_info
{
	int				file_size;
	int				n_links;
	char			*pw_uid;
	char			*gw_gid;
	char			*name;
	t_mode			*chmod;
	t_time			*clock;
}					t_info;

void				get_time(char *name, t_time **t, char *path);
int					get_mon(char *str);
t_list				*ft_ls_t(t_list **ls, char *path);
void				ft_arrange_t(t_list **temp, t_list **ls, char *path);
int					ft_time_compare(char *temp, char *str, char *path);
void				ft_ls(char *path);
void				ft_ls2(int argc, char **argv);
int					ft_ls_flags(char **argv, char *path);
void				ft_read(t_list **ls, struct dirent *dp, DIR *d);
void				ft_ls_a(t_list **ls);
void				ft_search_flags(char *str, t_list **ls, char *path);
t_list				*ft_ls_r(t_list **ls, int size);
void				ft_print_ls(t_list *ls);
void				ft_ls_flags2(int argc, char **argv);
void				ft_print_l(t_list *ls, char *str);
int					ft_get_info(t_info **file, t_list **ls, char *str);
char				*ft_mode(char c);
void				ft_get_chmod(t_mode **chmod, char *str);
char				ft_dir(int c);
void				ft_print_info(t_info **f);
int					ft_list_size(t_list *ls);
t_list				*ft_arrange(t_list **ls);
void				ft_compare_name(t_list **temp, t_list **ls);
char				*ft_get_path(char *path, char *name);
void				ft_search_dir(char **argv, char *path, t_list **ls);
int					ls_search(char *str);
int					ls_rec_search(char *str);
void				ft_rev(t_list **temp, t_list **ls);
void				ft_dir_ent(char *path, t_list **ls, char **argv);
void				ft_l_size(t_list *ls, char *path);

#endif
