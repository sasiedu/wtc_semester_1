/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 11:28:01 by sasiedu           #+#    #+#             */
/*   Updated: 2016/06/16 17:53:20 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"

void		ft_print_ls(t_list *ls)
{
	int		size;
	int		i;
	t_list	*temp;

	temp = ls;
	size = ft_list_size(ls);
	i = 0;
	while (i < size)
	{
		ft_printf("%s\n", temp->content);
		temp = temp->next;
		i++;
	}
}

int			ft_list_size(t_list *ls)
{
	int			size;
	t_list		*temp;

	size = 0;
	temp = ls;
	while (temp != NULL)
	{
		size++;
		temp = temp->next;
	}
	return (size);
}
