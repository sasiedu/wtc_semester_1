/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_join.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 17:21:38 by sasiedu           #+#    #+#             */
/*   Updated: 2016/07/24 18:01:29 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_array_join(char **tab, char *line)
{
	size_t	size;
	size_t	i;
	char	**new;

	size = ft_array_size(tab);
	if ((new = (char **)malloc(sizeof(char *) * size + 1)) == NULL)
		return (NULL);
	i = 0;
	while (i < size)
	{
		new[i] = ft_strdup(tab[i]);
		i++;
	}
	new[i] = ft_strdup(line);
	new[i + 1] = NULL;
	return (new);
}
